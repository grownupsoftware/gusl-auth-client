package gusl.auth.location;

import gusl.auth.model.geo.DeviceGeoLocationDataDO;
import org.jvnet.hk2.annotations.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class GeoDeviceLocationServiceImpl implements GeoDeviceLocationService {

    private static final String GEO_LATITUDE = "latitude";
    private static final String GEO_LONGITUDE = "longitude";
    private static final String GEO_ACCURACY = "accuracy";

    @Override
    public DeviceGeoLocationDataDO getDeviceGeoLocationData(HttpServletRequest request) {
        DeviceGeoLocationDataDO locationData = new DeviceGeoLocationDataDO();

        String value = request.getHeader(GEO_LATITUDE);
        if (value != null) {
            try {
                Double latitude = Double.valueOf(value);
                locationData.setLatitude(latitude);
                locationData.setPresent(true);
            } catch (NumberFormatException ex) {
                // ignore
            }
        } else {
            locationData.setPresent(false);
            return locationData;
        }

        value = request.getHeader(GEO_LONGITUDE);
        if (value != null) {
            try {
                Double longitude = Double.valueOf(value);
                locationData.setLongitude(longitude);
            } catch (NumberFormatException ex) {
                // ignore
            }
        }
        value = request.getHeader(GEO_ACCURACY);
        if (value != null) {
            try {
                Double accuracy = Double.valueOf(value);
                locationData.setAccuracy(accuracy);
            } catch (NumberFormatException ex) {
                // ignore
            }
        }

        return locationData;

    }

}
