package gusl.auth.location;

import gusl.auth.model.geo.DeviceGeoLocationDataDO;
import org.jvnet.hk2.annotations.Contract;

import javax.servlet.http.HttpServletRequest;

@Contract
public interface GeoDeviceLocationService {

    DeviceGeoLocationDataDO getDeviceGeoLocationData(HttpServletRequest request);
}
