package gusl.auth.location;

import gusl.auth.model.geo.IpGeoLocationDataDO;
import org.jvnet.hk2.annotations.Contract;

import java.net.InetAddress;

/**
 * @author dhudson
 */
@Contract
public interface GeoIpLocationService {

    /**
     * Get the ISO alpha-2 country code that the ip address is in.
     *
     * @param ipAddress
     * @return ISO code
     */
    String getCountryCode(String ipAddress);

    /**
     * Get the ISO alpha-2 country code that the ip address is in.
     *
     * @param ipAddress
     * @return ISO code
     */
    String getCountryCode(InetAddress ipAddress);

    /**
     * Get the detailed information about the country.
     *
     * @param ipAddress
     * @return country information or null
     */
    IpGeoLocationDataDO getCountryDetail(InetAddress ipAddress);
}
