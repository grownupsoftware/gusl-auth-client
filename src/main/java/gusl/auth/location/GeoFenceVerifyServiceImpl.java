package gusl.auth.location;

import gusl.auth.client.AuthClient;
import gusl.auth.model.geo.GeoFenceResponse;
import gusl.auth.model.geo.IpGeoLocationDataDO;
import gusl.auth.model.geofence.CIDRDO;
import gusl.auth.model.geofence.GeoFenceDO;
import gusl.core.eventbus.OnEvent;
import gusl.core.networking.CIDR;
import gusl.core.networking.Firewall;
import gusl.model.exceptions.IPRestrictedException;
import gusl.model.systemconfig.SystemConfigCacheEvent;
import gusl.node.application.events.CachesLoadedEvent;
import gusl.node.transport.HttpUtils;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static gusl.core.utils.SystemPropertyUtils.isRestricted;
import static java.util.Objects.nonNull;

@CustomLog
@Service
public class GeoFenceVerifyServiceImpl implements GeoFenceVerifyService {

    @Inject
    private GeoIpLocationService theGeoIpService;

    @Inject
    private AuthClient theAuthClient;

    private final Firewall theFirewall;

    private GeoFenceDO theGeoFence;

    public GeoFenceVerifyServiceImpl() {
        theFirewall = new Firewall();
    }

    public Optional<String> getNetworkName(String address) {
        try {
            final CIDR cidr = theFirewall.whichCIDRInWhiteList(address);
            return nonNull(cidr) ? Optional.of(cidr.getName()) : Optional.empty();
        } catch (UnknownHostException e) {
            logger.warn("Failed to get network name for [{}]", address);
            return Optional.empty();
        }
    }

    @Override
    public void setAuthClient(AuthClient client) {
        theAuthClient = client;
    }

    public AuthClient getAuthClient() {
        return theAuthClient;
    }

    @OnEvent(order = 70)
    public void handleSystemConfigChange(SystemConfigCacheEvent event) {
        reloadFirewall();
    }

    @OnEvent
    public void handleSystemStartedEvent(CachesLoadedEvent event) {
        // Lets fetch the GeoFence from the system config
        reloadFirewall();
    }

    private void reloadFirewall() {
        try {
            final GeoFenceDO active = getAuthClient().getActiveGeoFence().get();
            theFirewall.setWhiteList(createCIDRsFrom(active.getWhiteListCidrs()));
            theFirewall.setBlackList(createCIDRsFrom(active.getBlackListCidrs()));
            theGeoFence = active;

            logger.debug("Firewall: White {}, Black {} Geo Fence {}", theFirewall.getWhiteList(),
                    theFirewall.getBlackList(), theGeoFence.getCountryCodes());
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Failed to reload firewall", e);
        }
    }

    @Override
    public GeoFenceResponse checkGeoFence(HttpServletRequest servletRequest) {
        GeoFenceResponse response = new GeoFenceResponse();
        InetAddress clientAddress = HttpUtils.getRemoteIpAddress(servletRequest);

        response.setAddress(clientAddress);
        response.setHostname(clientAddress.getHostAddress());

        boolean restricted = isRestricted();

        // Only do this if restricted
        if (restricted) {
            if (theFirewall.isAddressInBlackList(clientAddress)) {
                throw new IPRestrictedException("Blacklisted", clientAddress.getHostAddress());
            }

            CIDR cidr = theFirewall.whichCIDRInWhiteList(clientAddress);
            if (cidr != null) {
                // We have found one, so return it
                response.setCidr(cidr);
            }
        }
        // Passed the firewall rules ..
        // Has cloudflare got the country for us
        String countryCode = servletRequest.getHeader(HttpUtils.CF_COUNTRY_HEADER_NAME);
        IpGeoLocationDataDO locationData = theGeoIpService.getCountryDetail(clientAddress);
        response.setLocationData(locationData);

        if (countryCode == null) {
            if (locationData != null) {
                countryCode = locationData.getCountryCode();
            }
        }

        response.setCountryCode(countryCode);

        if (restricted) {
            if (countryCode != null) {
                if (!theGeoFence.getCountryCodes().contains(countryCode)) {
                    throw new IPRestrictedException(countryCode, response.getHostname());
                }
            }
        }

        return response;
    }

    private List<CIDR> createCIDRsFrom(List<CIDRDO> cidrdos) {
        if (cidrdos == null) {
            return Collections.emptyList();
        }

        List<CIDR> cidr = new ArrayList<>(cidrdos.size());
        cidrdos.forEach(cidrdo -> {
            try {
                cidr.add(new CIDR(cidrdo.getCidr(), cidrdo.getName()));
            } catch (UnknownHostException ex) {
                logger.warn("Error with CIDR {}", cidrdo, ex);
            }
        });

        return cidr;
    }
}
