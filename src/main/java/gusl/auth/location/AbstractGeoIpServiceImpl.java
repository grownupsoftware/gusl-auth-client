package gusl.auth.location;

import com.maxmind.db.Reader;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import gusl.auth.model.geo.IpGeoLocationDataDO;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;
import gusl.core.utils.Utils;
import gusl.node.converter.ModelConverter;
import gusl.node.jersey.JerseyHttpConfig;
import gusl.node.jersey.JerseyNodeClient;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static gusl.node.jersey.JerseyHttpConfig.*;
import static java.util.Objects.nonNull;

public abstract class AbstractGeoIpServiceImpl implements GeoIpLocationService {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    protected DatabaseReader reader = null;

    @Inject
    private ModelConverter theModelConverter;

    private static final String GEO_CITY_FILE = "GeoIP2-City.mmdb";

    protected String getDatabaseName() {
        return GEO_CITY_FILE;
    }

    public void loadDatabaseFromFile(File databaseFile) {
        try {
            try (InputStream is = new FileInputStream(databaseFile)) {
                reader = new DatabaseReader.Builder(is).fileMode(Reader.FileMode.MEMORY).build();
            }
            logger.info("Initialised the GEO IP Location Service.");
        } catch (IOException ex) {
            logger.warn("Unable to load GEO IP database", ex);
        }
    }

    protected void loadLocalCopy() {
        BackgroundThreadPoolExecutor.submit(() -> {
            try {
                loadDatabaseFromFile(IOUtils.getResourceAsFile(GEO_CITY_FILE, this.getClass().getClassLoader()));
            } catch (IOException ex) {
                logger.warn("Unable to load GEO IP database from resource", ex);
            }
        });
    }


    protected void loadGUSLCopy() {
        BackgroundThreadPoolExecutor.submit(() -> {
            try {
                String location = SystemPropertyUtils.getRepositoryLocation();
                if (StringUtils.isBlank(location)) {
                    location = System.getProperty("java.io.tmpdir", "/tmp");
                }

                File repoLocation = new File(location);

                if (!repoLocation.exists()) {
                    logger.info("Creating Application Repo {}", repoLocation.getAbsolutePath());
                    repoLocation.mkdirs();
                }

                File database = new File(repoLocation, GEO_CITY_FILE);
                if (!database.exists()) {
                    JerseyNodeClient client = new JerseyNodeClient(JerseyHttpConfig.builder()
                            .socketConfig(defaultSocketConfig())
                            .properties(defaultClientProperties())
                            .requiresConnectionPool(false)
                            .readTimeout(READ_TIMEOUT_DEFAULT)
                            .connectionTimeout(CONNECT_TIMEOUT_DEFAULT)
                            .maxConnections(MAX_CONNECTIONS_DEFAULT)
                            .build());

                    WebTarget webTarget = client.getClient().target("https://storage.googleapis.com/gusl-images/GeoIP2-City.mmdb");
                    Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_OCTET_STREAM_TYPE);

                    logger.info("Downloading database ..");
                    Response response = builder.get();

                    FileOutputStream fos = new FileOutputStream(database);
                    IOUtils.copyStream(response.readEntity(InputStream.class), fos);
                    logger.info("Database downloaded ..");
                    response.close();
                    client.close();
                }

                loadDatabaseFromFile(database);
            } catch (IOException ex) {
                logger.warn("Unable to load GEO IP database", ex);
            }
        });
    }

    /**
     * Get the ISO alpha-2 country code that the ip address is in.
     *
     * @param ipAddress
     * @return
     */
    @Override
    public String getCountryCode(String ipAddress) {
        String countryCode = null;
        try {
            InetAddress inet = InetAddress.getByName(ipAddress);

            if (ipAddress != null && !ipAddress.startsWith("10.") && !ipAddress.startsWith("127.")) {
                countryCode = getCountryCode(inet);
            }
        } catch (UnknownHostException e) {
            logger.debug("Error getting country information from Maxmind GEO IP Database for IP {}.",
                    ipAddress, e);
        }
        return countryCode;
    }

    /**
     * Get the ISO alpha-2 country code that the ip address is in.
     *
     * @param ipAddress
     * @return
     */
    @Override
    public String getCountryCode(InetAddress ipAddress) {

        String countryCode = null;
        if (isIpAddressSearchable(ipAddress)) {
            try {
                CityResponse response = reader.city(ipAddress);
                if (response != null) {
                    com.maxmind.geoip2.record.Country country = response.getCountry();
                    if (country != null) {
                        countryCode = country.getIsoCode();
                    }
                }
            } catch (IOException | GeoIp2Exception e) {
                logger.debug("ERR001: Error getting country information from Maxmind GEO IP Database for IP {}.", ipAddress.getHostAddress(), e);
            }
        }
        return countryCode;
    }

    @Override
    public IpGeoLocationDataDO getCountryDetail(InetAddress ipAddress) {
        IpGeoLocationDataDO data = new IpGeoLocationDataDO();
        data.setIpAddress(ipAddress);

        if (nonNull(reader)) {
            if (isIpAddressSearchable(ipAddress)) {
                try {
                    theModelConverter.merge(reader.city(ipAddress), data);
                } catch (NullPointerException | IOException | GeoIp2Exception ex) {
                    logger.warn("reader: {}", reader);
                    logger.warn("ipAddress: {}", ipAddress);
                    logger.warn("Unable to get Country Detail for ip {}", ipAddress, ex);
                    data.setLocalAddress(true);
                }
            } else {
                data.setLocalAddress(true);
            }
        }

        return data;
    }

    private boolean isIpAddressSearchable(InetAddress address) {
        if (address == null) {
            return false;
        }

        return !Utils.isLocalAddress(address);
    }
}
