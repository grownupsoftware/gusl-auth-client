package gusl.auth.location;

import gusl.auth.client.AuthClient;
import gusl.auth.model.geo.GeoFenceResponse;
import org.jvnet.hk2.annotations.Contract;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Contract
public interface GeoFenceVerifyService {

    GeoFenceResponse checkGeoFence(HttpServletRequest servletRequest);

    Optional<String> getNetworkName(String address);

    void setAuthClient(AuthClient client);
}
