package gusl.auth.roles;

import gusl.auth.errors.AuthenticationErrors;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.ClassUtils;
import gusl.model.nodeconfig.NodeType;
import gusl.model.role.RoleDefinitions;
import gusl.model.role.RoleGrouping;
import org.jvnet.hk2.annotations.Service;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;

@Service
public class PermissionCollectorImpl implements PermissionCollector {
    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private Reflections theReflections;
    private String[] thePackagePrefixes;
    List<RoleGrouping> thePermissionGroupings;

    private NodeType theNodeType;

    @Override
    public List<RoleGrouping> getPermissionGroupings() {
        return thePermissionGroupings;
    }

    @Override
    public void log() {
        StringBuilder builder = new StringBuilder();

        safeStream(thePermissionGroupings).forEach(roleGroup -> {
            builder.append("\t").append(roleGroup.getName()).append("\n");
            safeStream(roleGroup.getRoles()).forEach(role -> builder.append("\t\t").append(role).append("\n"));
        });
        logger.info("Permission Groupings: \n{}", builder.toString());
    }

    @Override
    public NodeType getNodeType() {
        return theNodeType;
    }

    @Override
    public void initialise(NodeType nodeType, String... packagePrefixes) throws GUSLErrorException {
        theNodeType = nodeType;
        thePackagePrefixes = packagePrefixes;
        bootstrap();
        final Set<Class<? extends RoleDefinitions>> roleDefinitions = theReflections.getSubTypesOf(RoleDefinitions.class);

        thePermissionGroupings = safeStream(roleDefinitions)
                .map(roleDefinition -> {
                    try {
                        final RoleDefinitions definition = ClassUtils.getNewInstance(roleDefinition);
                        return definition.getRoles();
                    } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                        logger.error("Error creating instance of {}", roleDefinition.getClass(), e);
                        return null;
                    }
                }).filter(Objects::nonNull)
                .collect(Collectors.toUnmodifiableList());

    }

    private synchronized void bootstrap() throws GUSLErrorException {
        if (isNull(theReflections)) {
            if (isNull(thePackagePrefixes)) {
                throw new GUSLErrorException(AuthenticationErrors.NO_PACKAGE_PREFIX_DEFINED.getError());
            }

            ConfigurationBuilder builder = new ConfigurationBuilder();
            safeStream(thePackagePrefixes).forEach(pkg -> {
                builder.addUrls(ClasspathHelper.forPackage(pkg));
            });
            builder.addScanners(new TypeAnnotationsScanner());
            try {
                theReflections = new Reflections(builder);
            } catch (IllegalStateException ex) {
                // Try again ...
                theReflections = new Reflections(builder);
            }
        }
    }

}
