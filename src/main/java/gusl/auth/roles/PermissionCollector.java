package gusl.auth.roles;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.nodeconfig.NodeType;
import gusl.model.role.RoleGrouping;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

@Contract
public interface PermissionCollector {

    void initialise(NodeType nodeType, String... packagePrefixes) throws GUSLErrorException;

    NodeType getNodeType();

    List<RoleGrouping> getPermissionGroupings();

    void log();
}
