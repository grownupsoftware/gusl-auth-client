package gusl.auth.roles;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

import java.util.Set;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toUnmodifiableList;

/**
 * So we create one of these per login.
 * <p>
 * This means that if you change the roles for the user, it will not take effect
 * until they re-login.
 *
 * @author dhudson
 */
public class RoleBasedSecurityContext extends GUSLSecurityContext {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private Set<String> permissions;

    public RoleBasedSecurityContext(String name, Set<String> permissions) {
        super(name);
        this.permissions = permissions;
    }

    @Override
    public boolean isUserInRole(String permission) {
        if (nonNull(permissions) && nonNull(permission)) {
            final boolean allowed = permissions.contains(permission.toUpperCase());
            if (allowed) {
                return true;
            }
        }
        logger.info("Name: {} Permission: {} not found in {}",
                getUserPrincipal().getName()
                , permission,
                safeStream(permissions).sorted().collect(toUnmodifiableList()));
        return false;
    }
}
