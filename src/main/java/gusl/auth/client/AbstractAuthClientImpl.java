package gusl.auth.client;

import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.model.geofence.GeoFenceDO;
import gusl.auth.model.geofence.GeoFencesDO;
import gusl.auth.model.geofence.PagedGeoFenceDO;
import gusl.auth.model.loadcache.LoadBOAuthCacheResponseDO;
import gusl.auth.model.loadcache.LoadFEAuthCacheResponseDO;
import gusl.auth.model.login.AuthLoginResponseDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.LogoutResponseDO;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.register.RegisterRequestDO;
import gusl.auth.model.register.RegisterResponseDO;
import gusl.auth.model.roles.PagedRoleDO;
import gusl.auth.model.roles.RoleDO;
import gusl.auth.model.roles.RolesDO;
import gusl.auth.model.security.*;
import gusl.auth.model.session.*;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.model.dataservice.GetStringIdRequestDO;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.model.nodeconfig.NodeType;
import gusl.model.role.NodeRolesNotificationDO;
import gusl.model.role.NodeRolesRequestDO;
import gusl.model.role.NodeRolesResponseDO;
import gusl.model.success.SuccessResponseDO;
import gusl.node.client.AbstractNodeClient;
import gusl.query.QueryParams;

import java.util.concurrent.CompletableFuture;

public abstract class AbstractAuthClientImpl extends AbstractNodeClient implements AuthClient {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    public AbstractAuthClientImpl(NodeType nodeType) {
        super(nodeType);
    }

    @Override
    public CompletableFuture<LoadBOAuthCacheResponseDO> loadCacheForBackOffice() {
        return sendRequest(createHttpNodeRequest("cache/v1/load/bo", null, LoadBOAuthCacheResponseDO.class));
    }

    @Override
    public CompletableFuture<LoadFEAuthCacheResponseDO> loadCacheForFE() {
        return sendRequest(createHttpNodeRequest("cache/v1/load/fe", null, LoadFEAuthCacheResponseDO.class));
    }

//
//    @Override
//    public CompletableFuture<ILoggedInUser> updateLoggedInUserDetails(AuditableDTO<AdminLoggedInUserDO> request) {
//        return sendRequest(createHttpNodeRequest("backofficesession/v1/create-session", request, AdminLoggedInUserDO.class));
//    }

    @Override
    public CompletableFuture<LogoutResponseDO> logoutSession(SessionDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/logout", request, LogoutResponseDO.class));
    }

    @Override
    public CompletableFuture<RegisterResponseDO> register(RegisterRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/register", request, RegisterResponseDO.class));
    }

    @Override
    public CompletableFuture<AuthLoginResponseDO> signIn(SignInRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/login", request, AuthLoginResponseDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> insertUserSecurity(UserSecurityDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/insert", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> updateUserSecurity(AuditableDTO<UserSecurityDO> request) {
        return sendRequest(createHttpNodeRequest("auth/v1/update", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> getUserSecurityByKey(SignInRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/get-by-key", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> getUserSecurityById(GetStringIdRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/get-by-id", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> getUserSecurityByUser(SignInRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/get-by-user", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> getUserSecurityByUsername(GetUserByUsernameRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/get-by-username", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<CreateSessionResponseDO> createSession(CreateSessionRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/create-session", request, CreateSessionResponseDO.class));
    }

    @Override
    public CompletableFuture<SuccessResponseDO> changePassword(ChangePasswordRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/change-password", request, SuccessResponseDO.class));
    }

    @Override
    public CompletableFuture<SuccessResponseDO> changeUsername(ChangeUsernameRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/change-username", request, SuccessResponseDO.class));
    }

    @Override
    public CompletableFuture<PagedDAOResponse<GeoFenceDO>> getPagedGeoFences(QueryParams queryParams) {
        return sendRequest(createHttpNodeRequest("geofence/v1/paged-query", queryParams, PagedDAOResponse.class, GeoFenceDO.class));
    }

    @Override
    public CompletableFuture<GeoFencesDO> getAllGeoFence() {
        return sendRequest(createHttpNodeRequest("geofence/v1/all", null, GeoFencesDO.class));
    }

    @Override
    public CompletableFuture<GeoFenceDO> getGeoFence(GetStringIdRequestDO request) {
        return sendRequest(createHttpNodeRequest("geofence/v1/get", request, GeoFenceDO.class));
    }

    @Override
    public CompletableFuture<GeoFenceDO> getActiveGeoFence() {
        return sendRequest(createHttpNodeRequest("geofence/v1/active", null, GeoFenceDO.class));
    }

    @Override
    public CompletableFuture<GeoFenceDO> insertGeoFence(AuditableDTO<GeoFenceDO> request) {
        return sendRequest(createHttpNodeRequest("geofence/v1/insert", request, GeoFenceDO.class));
    }

    @Override
    public CompletableFuture<PagedGeoFenceDO> getAllPagedGeoFence(PageRequest request) {
        return sendRequest(createHttpNodeRequest("geofence/v1/paged", request, PagedGeoFenceDO.class));
    }

    @Override
    public CompletableFuture<GeoFenceDO> updateGeoFence(AuditableDTO<GeoFenceDO> request) {
        return sendRequest(createHttpNodeRequest("geofence/v1/update", request, GeoFenceDO.class));
    }

    @Override
    public CompletableFuture<SessionsDO> getAllForUser(SessionSelectAllDO request) {
        return sendRequest(createHttpNodeRequest("session/v1/get-all-for-bettor", request, SessionsDO.class));
    }

    @Override
    public CompletableFuture<SessionsDO> getSessions(QueryParams request) {
        return sendRequest(createHttpNodeRequest("session/v1/query", request, SessionsDO.class));
    }

    @Override
    public CompletableFuture<SessionDO> changeSessionStatus(SessionStatusChangeRequestDO requestDo) {
        return sendRequest(createHttpNodeRequest("session/v1/status", requestDo, SessionDO.class));
    }

//    @Override
//    public CompletableFuture<PagedSessionDO> getPagedSessions(PageRequest request) {
//        return sendRequest(createHttpNodeRequest("session/v1/paged", request, PagedSessionDO.class));
//    }

    @Override
    public CompletableFuture<PagedDAOResponse<SessionDO>> getPagedSessions(QueryParams queryParams) {
        return sendRequest(createHttpNodeRequest("session/v1/paged-query", queryParams, PagedDAOResponse.class, SessionDO.class));
    }

    @Override
    public CompletableFuture<SessionDO> getSession(SessionGetRequestDO request) {
        return sendRequest(createHttpNodeRequest("session/v1/get", request, SessionDO.class));
    }

    @Override
    public CompletableFuture<PagedRoleDO> getAllPagedRole(PageRequest request) {
        return sendRequest(createHttpNodeRequest("role/v1/paged", request, PagedRoleDO.class));
    }

    @Override
    public CompletableFuture<PagedDAOResponse<RoleDO>> getAllRolesPaged(QueryParams queryParams) {
        return sendRequest(createHttpNodeRequest("role/v1/query-params", queryParams, PagedDAOResponse.class, RoleDO.class));
    }

    @Override
    public CompletableFuture<RolesDO> getAllRoles() {
        return sendRequest(createHttpNodeRequest("role/v1/all", null, RolesDO.class));
    }

    @Override
    public CompletableFuture<RoleDO> getRole(GetStringIdRequestDO request) {
        return sendRequest(createHttpNodeRequest("role/v1/get", request, RoleDO.class));
    }

    @Override
    public CompletableFuture<RoleDO> insertRole(AuditableDTO<RoleDO> request) {
        return sendRequest(createHttpNodeRequest("role/v1/insert", request, RoleDO.class));
    }

    @Override
    public CompletableFuture<RoleDO> updateRole(AuditableDTO<RoleDO> request) {
        return sendRequest(createHttpNodeRequest("role/v1/update", request, RoleDO.class));
    }

    @Override
    public CompletableFuture<SuccessResponseDO> updateNodeRoles(NodeRolesNotificationDO request) {
        return sendRequest(createHttpNodeRequest("role/v1/update-node-roles", request, SuccessResponseDO.class));
    }

    @Override
    public CompletableFuture<NodeRolesResponseDO> getNodeRoles(NodeRolesRequestDO request) {
        return sendRequest(createHttpNodeRequest("role/v1/get-node-roles", request, NodeRolesResponseDO.class));
    }

    @Override
    public CompletableFuture<OldSessionTokenResponseDO> getOldToken(OldSessionTokenRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/old-token", request, OldSessionTokenResponseDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> renewValidationKey(RenewValidationKeyRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/renew-validation-key", request, UserSecurityDO.class));
    }

    @Override
    public CompletableFuture<UserSecurityDO> verifyValidationKey(VerifyValidationKeyRequestDO request) {
        return sendRequest(createHttpNodeRequest("auth/v1/verify-validation-key", request, UserSecurityDO.class));
    }

}
