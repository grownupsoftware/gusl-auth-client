package gusl.auth.client;

import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.model.geofence.GeoFenceDO;
import gusl.auth.model.geofence.GeoFencesDO;
import gusl.auth.model.geofence.PagedGeoFenceDO;
import gusl.auth.model.loadcache.LoadBOAuthCacheResponseDO;
import gusl.auth.model.loadcache.LoadFEAuthCacheResponseDO;
import gusl.auth.model.login.AuthLoginResponseDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.LogoutResponseDO;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.register.RegisterRequestDO;
import gusl.auth.model.register.RegisterResponseDO;
import gusl.auth.model.roles.PagedRoleDO;
import gusl.auth.model.roles.RoleDO;
import gusl.auth.model.roles.RolesDO;
import gusl.auth.model.security.*;
import gusl.auth.model.session.*;
import gusl.model.dataservice.GetStringIdRequestDO;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.model.role.NodeRolesNotificationDO;
import gusl.model.role.NodeRolesRequestDO;
import gusl.model.role.NodeRolesResponseDO;
import gusl.model.success.SuccessResponseDO;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Contract;

import java.util.concurrent.CompletableFuture;

@Contract
public interface AuthClient {

    // ---------------------------------- Cache
    CompletableFuture<LoadBOAuthCacheResponseDO> loadCacheForBackOffice();

    CompletableFuture<LoadFEAuthCacheResponseDO> loadCacheForFE();

    // ---------------------------------- Other

    // CompletableFuture<ILoggedInUser> updateLoggedInUserDetails(AuditableDTO<ILoggedInUser> requestDo) throws GUSLErrorException;

    // ---------------------------------- security

    CompletableFuture<RegisterResponseDO> register(RegisterRequestDO request);

    CompletableFuture<AuthLoginResponseDO> signIn(SignInRequestDO request);

    CompletableFuture<UserSecurityDO> insertUserSecurity(UserSecurityDO request);

    CompletableFuture<UserSecurityDO> updateUserSecurity(AuditableDTO<UserSecurityDO> request);

    CompletableFuture<UserSecurityDO> getUserSecurityByKey(SignInRequestDO request);

    CompletableFuture<UserSecurityDO> getUserSecurityById(GetStringIdRequestDO request);

    CompletableFuture<UserSecurityDO> getUserSecurityByUser(SignInRequestDO request);

    CompletableFuture<UserSecurityDO> getUserSecurityByUsername(GetUserByUsernameRequestDO request);

    // ---------------------------------- Session
//    CompletableFuture<PagedSessionDO> getPagedSessions(PageRequest request);

    CompletableFuture<PagedDAOResponse<SessionDO>> getPagedSessions(QueryParams queryParams);

    CompletableFuture<SessionDO> getSession(SessionGetRequestDO request);

    CompletableFuture<SessionsDO> getAllForUser(SessionSelectAllDO request);

    CompletableFuture<SessionsDO> getSessions(QueryParams request);

    CompletableFuture<SessionDO> changeSessionStatus(SessionStatusChangeRequestDO requestDo);

    CompletableFuture<CreateSessionResponseDO> createSession(CreateSessionRequestDO request);

    CompletableFuture<SuccessResponseDO> changePassword(ChangePasswordRequestDO request);

    CompletableFuture<SuccessResponseDO> changeUsername(ChangeUsernameRequestDO request);

    CompletableFuture<LogoutResponseDO> logoutSession(SessionDO session);

    // ---------------------------------- GeoFence

    CompletableFuture<PagedDAOResponse<GeoFenceDO>> getPagedGeoFences(QueryParams queryParams);

    CompletableFuture<GeoFencesDO> getAllGeoFence();

    CompletableFuture<GeoFenceDO> getGeoFence(GetStringIdRequestDO request);

    CompletableFuture<GeoFenceDO> getActiveGeoFence();

    CompletableFuture<GeoFenceDO> insertGeoFence(AuditableDTO<GeoFenceDO> request);

    CompletableFuture<PagedGeoFenceDO> getAllPagedGeoFence(PageRequest request);

    CompletableFuture<GeoFenceDO> updateGeoFence(AuditableDTO<GeoFenceDO> request);

    // ---------------------------------- Role
    CompletableFuture<PagedRoleDO> getAllPagedRole(PageRequest request);

    CompletableFuture<PagedDAOResponse<RoleDO>> getAllRolesPaged(QueryParams params);

    CompletableFuture<RolesDO> getAllRoles();

    CompletableFuture<RoleDO> getRole(GetStringIdRequestDO request);

    CompletableFuture<RoleDO> insertRole(AuditableDTO<RoleDO> request);

    CompletableFuture<RoleDO> updateRole(AuditableDTO<RoleDO> request);

    CompletableFuture<SuccessResponseDO> updateNodeRoles(NodeRolesNotificationDO request);

    CompletableFuture<NodeRolesResponseDO> getNodeRoles(NodeRolesRequestDO request);

    CompletableFuture<OldSessionTokenResponseDO> getOldToken(OldSessionTokenRequestDO request);

    CompletableFuture<UserSecurityDO> verifyValidationKey(VerifyValidationKeyRequestDO request);

    CompletableFuture<UserSecurityDO> renewValidationKey(RenewValidationKeyRequestDO request);

}
