package gusl.auth.cache.session;

import gusl.auth.cache.ExpiringCache;
import gusl.auth.model.session.Session;
import gusl.auth.model.session.SessionCacheEvent;
import gusl.auth.model.session.SessionCacheKey;
import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.cache.CacheAction;
import gusl.model.event.HouseKeepingEvent;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.util.List;
import java.util.Optional;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
@Service
public class SessionCacheImpl extends ExpiringCache<SessionCacheKey, Session> implements SessionCache {

    public SessionCacheImpl() {
        super("SessionCache", ExpiringCache.CACHE_TTL_TIME, ExpiringCache.CACHE_TTL_TIMEUNIT, ExpiringCache.CACHE_CLEAN_TIME, ExpiringCache.CACHE_CLEAN_TIMEUNIT);
    }

    @Override
    public void populateCache(List<? extends Session> list) {
        super.populateCache(list);
        fireEvent(new SessionCacheEvent(CacheAction.POPULATE, null));
    }

    @OnEvent
    public void cacheListener(SessionCacheEvent event) {
        super.updateCache(event);
        if (nonNull(event.getDataObject())
                && nonNull(event.getDataObject().getStatus())
                && event.getDataObject().getStatus().canRemoveFromCache()) {
            super.remove(event.getDataObject());
        }
    }

    @Override
    public Session fetch(SessionCacheKey key) throws GUSLErrorException {
        return null;
        // return getAuthClient().getSession(SessionGetRequestDO.builder().bettorId(key.getBettorId()).id(key.getId()).build());
    }

    @OnEvent()
    public void handleHousekeepingEvent(HouseKeepingEvent event) throws GUSLErrorException {

    }

    @Override
    public Optional<Session> getSessionBySessionToken(String token) {
        if (isNull(token)) {
            return Optional.empty();
        }
        return safeStream(getAll())
                .filter(session -> nonNull(session.getSessionToken()))
                .filter(session -> session.getSessionToken().equals(token))
                .findFirst();
    }
}
