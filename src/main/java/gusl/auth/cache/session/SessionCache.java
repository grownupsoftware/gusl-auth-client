package gusl.auth.cache.session;

import gusl.auth.model.session.Session;
import gusl.auth.model.session.SessionCacheKey;
import gusl.cache.KeyDoCache;
import org.jvnet.hk2.annotations.Contract;

import java.util.Optional;

@Contract
public interface SessionCache extends KeyDoCache<SessionCacheKey, Session> {

    void add(Session session);

    Optional<Session> getSessionBySessionToken(String token);

}
