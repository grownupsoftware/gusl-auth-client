package gusl.auth.cache.role;

import gusl.auth.client.AuthClient;
import gusl.cache.AbstractIdentifiableStringCache;
import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.JsonUtils;
import gusl.model.role.NodeRolesNotificationDO;
import gusl.model.role.NodeRolesNotificationEvent;
import gusl.model.role.NodeRolesRequestDO;
import gusl.model.role.NodeRolesResponseDO;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

import static gusl.node.transport.HttpUtils.waitForFuture;

@Service
@CustomLog
public class NodeRoleCacheImpl extends AbstractIdentifiableStringCache<NodeRolesNotificationDO> implements NodeRoleCache {

    @Inject
    private AuthClient theAuthClient;

    public void loadCache() throws GUSLErrorException {
        final NodeRolesResponseDO response = waitForFuture(theAuthClient.getNodeRoles(NodeRolesRequestDO.builder().build()));
        logger.debug("NodeRoles: {}", JsonUtils.prettyPrint(response));
        populateCache(response.getNodeRoles());
    }

    @OnEvent
    public void cacheListener(NodeRolesNotificationEvent event) {
        logger.debug("NodeRole Notification: {}", JsonUtils.prettyPrint(event));
        super.add(event.getContent());
    }

}
