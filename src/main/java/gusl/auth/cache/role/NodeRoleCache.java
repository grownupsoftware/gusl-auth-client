package gusl.auth.cache.role;

import gusl.cache.KeyDoCache;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.role.NodeRolesNotificationDO;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface NodeRoleCache  extends KeyDoCache<String, NodeRolesNotificationDO> {

    void loadCache() throws GUSLErrorException;

}
