package gusl.auth.cache.role;

import gusl.auth.model.roles.RoleCacheEvent;
import gusl.auth.model.roles.RoleDO;
import gusl.auth.model.roles.RoleHelper;
import gusl.auth.model.roles.RolesIdsDO;
import gusl.cache.AbstractIdentifiableStringCache;
import gusl.core.eventbus.OnEvent;
import gusl.core.utils.StringUtils;
import org.jvnet.hk2.annotations.Service;

import java.util.*;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class RoleCacheImpl extends AbstractIdentifiableStringCache<RoleDO> implements RoleCache {

    private final Set<String> theSystemWidePermissions = new HashSet<>();

    public RoleCacheImpl() {
        theSystemWidePermissions.add("SYSTEM_FORMS");
        theSystemWidePermissions.add("USER_PASSWORD_RESET");
    }

    @Override
    public Set<String> getPermissionsForRoles(final RolesIdsDO roleIds) {
        final Set<String> permissions = safeStream(isNull(roleIds) || isNull(roleIds.getIds()) ? new ArrayList<>() : roleIds.getIds())
                .map(roleId -> {
                    if (StringUtils.isNotBlank(roleId)) {
                        final RoleDO roleDO = get(roleId);
                        if (nonNull(roleDO) && nonNull(roleDO.getPermissions()) && nonNull(roleDO.getPermissions().getPermissions())) {
                            return roleDO.getPermissions().getPermissions();
                        }
                    }
                    return null;
                }).filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        // Add system-wide permissions
        permissions.addAll(theSystemWidePermissions);
        return permissions;
    }

    @Override
    public boolean isSuperUser(RolesIdsDO roleIds) {
        return Optional.ofNullable(roleIds)
                .map(roles -> roles.getIds().contains(RoleHelper.SUPER_USER_ID))
                .orElse(false);
    }

    @OnEvent
    public void cacheListener(RoleCacheEvent event) {
        super.updateCache(event);
    }

    public void addSystemWidePermission(String permission) {
        theSystemWidePermissions.add(permission);
    }

}

