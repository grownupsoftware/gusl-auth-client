package gusl.auth.cache.role;

import gusl.auth.model.roles.RoleDO;
import gusl.auth.model.roles.RolesIdsDO;
import gusl.cache.KeyDoCache;
import org.jvnet.hk2.annotations.Contract;

import java.util.Set;

@Contract
public interface RoleCache extends KeyDoCache<String, RoleDO> {

    Set<String> getPermissionsForRoles(RolesIdsDO roleIds);

    boolean isSuperUser(RolesIdsDO roleIds);

    public void addSystemWidePermission(String permission);
}

