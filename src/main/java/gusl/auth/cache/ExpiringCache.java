package gusl.auth.cache;

import gusl.cache.GuslExpiringCache;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.IdentifiableKey;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.node.converter.ModelConverter;
import gusl.node.eventbus.NodeEventBus;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.nonNull;

public abstract class ExpiringCache<K, DO extends IdentifiableKey<K>> extends GuslExpiringCache<K, DO> {

    @Inject
    private ModelConverter theModelConverter;

    @Inject
    private NodeEventBus theEventBus;

    public static final int CACHE_TTL_TIME = 10;
    public static final TimeUnit CACHE_TTL_TIMEUNIT = TimeUnit.DAYS;
    public static final int CACHE_CLEAN_TIME = 10;
    public static final TimeUnit CACHE_CLEAN_TIMEUNIT = TimeUnit.DAYS;

    public ExpiringCache(String cacheName, int ttlTime, TimeUnit ttlTimeUnit, int cleanUpTime, TimeUnit cleanUpTimeUnit) {
        super(cacheName, ttlTime, ttlTimeUnit, cleanUpTime, cleanUpTimeUnit);
        logger.debug("Creating cache: {} [{} {} {} {}]", cacheName, ttlTime, ttlTimeUnit, cleanUpTime, cleanUpTimeUnit);
    }

    @Override
    public void entryExpired(K key, DO value) {
        // override and throw an event if need be
        // logger.info("{} Expired key:[{}] value:{}", getCacheName(), key, value);
    }

    @Override
    public abstract DO fetch(K key) throws GUSLErrorException;

    protected String getAdminUser() {
        return "Cache";
    }

    public <T> boolean hasEvent(AbstractCacheActionEvent<T> event) {
        return nonNull(event) && nonNull(event.getCacheAction()) && (nonNull(event.getDataObject()) || CacheAction.CLEAR == event.getCacheAction());
    }

    public void fireEvent(final Object event) {
        theEventBus.post(event);
    }

}
