package gusl.auth.cors;

import gusl.node.providers.CORSProvider;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class CORSResponseFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        MultivaluedMap<String, String> requestHeaders = requestContext.getHeaders();
        if (requestHeaders.containsKey("Origin")) {
            CORSProvider.addCORSIfRequired(responseContext.getHeaders());
        }
    }

}
