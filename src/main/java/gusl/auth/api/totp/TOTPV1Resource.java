package gusl.auth.api.totp;

import gusl.auth.model.totp.QrCodeRequestDTO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.node.resources.AbstractBaseResource;
import gusl.node.transport.HttpUtils;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@Singleton
@Path(value = "totp/v1")
@PermitAll
public class TOTPV1Resource extends AbstractBaseResource {

    @Inject
    private TOTPService theService;

    @POST
    @Path(value = "qrcode")
    public void getQrCode(
            @Valid QrCodeRequestDTO request,
            @Suspended final AsyncResponse asyncResponse,
            @Context UriInfo uriInfo,
            @Context final HttpServletRequest servletRequest,
            @Context HttpServletResponse httpResponse,
            @HeaderParam(HttpUtils.USER_AGENT_HEADER_NAME) String userAgent
    ) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getCRCode(request));
    }

}
