package gusl.auth.api.totp;

import com.google.zxing.WriterException;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import gusl.auth.client.AuthClient;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.security.ScratchCodeDO;
import gusl.auth.model.security.ScratchCodesDO;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.model.totp.QRCodeGenerator;
import gusl.auth.model.totp.QrCodeRequestDTO;
import gusl.auth.model.totp.QrCodeResponseDTO;
import gusl.auth.model.users.frontend.UserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.security.InvalidOperationException;
import gusl.core.security.ObfuscatedStorage;
import gusl.core.security.PasswordStorage;
import gusl.model.errors.SystemErrors;
import gusl.node.resources.AbstractBaseResource;
import gusl.node.transport.HttpUtils;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.stream.Collectors;

import static gusl.core.utils.Utils.safeStream;

/**
 * @author dhudson
 * @since 07/10/2021
 */
@Service
public class TOTPServiceImpl extends AbstractBaseResource implements TOTPService {

    private GoogleAuthenticator theAuthenticator;

    @Inject
    private QRCodeGenerator theQRCodeGenerator;

    @Inject
    private AuthClient theAuthClient;

    public TOTPServiceImpl() {
        theAuthenticator = new GoogleAuthenticator();
        // will have to come back and try this
        // note: AbstractAuthenticationService and GoogleAuthenticator have new GoogleAuthenticator(). so needs to be consistent
//        theAuthenticator = new GoogleAuthenticator(new GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder()
//                .setHmacHashFunction(HmacHashFunction.HmacSHA1)
//                .setKeyRepresentation(KeyRepresentation.BASE32)
//                .setSecretBits(128)
//                .setTimeStepSizeInMillis(TimeUnit.SECONDS.toMillis(30))
//                .setWindowSize(3)
//                .setNumberOfScratchCodes(10)
//                .setCodeDigits(6)
//                .build());
    }

    @Override
    public UserSecurityDO generateUserKey(UserSecurityDO userSecurity) {
        GoogleAuthenticatorKey key = theAuthenticator.createCredentials();
        userSecurity.setAuthenticatorKey(key.getKey());
        userSecurity.setScratchCodes(ScratchCodesDO.builder()
                .codes(safeStream(key.getScratchCodes())
                        .map(scratchCode -> ScratchCodeDO.builder()
                                .used(false)
                                .code(saltValue(scratchCode.toString()))
                                .visibleCode(ObfuscatedStorage.encrypt(scratchCode.toString())) // <= one off visible by user
                                .build())
                        .collect(Collectors.toList()))
                .build());
        return userSecurity;
    }

    private String saltValue(String value) {
        try {
            return PasswordStorage.createHash(value);
        } catch (InvalidOperationException e) {
            logger.info("Failed to salt {}", value, e);
            return null;
        }
    }

    @Override
    public boolean validate(UserSecurityDO userSecurity, int code) {
        return theAuthenticator.authorize(userSecurity.getAuthenticatorKey(), code);
    }

    @Override
    public String getCRCode(String username, String authenticatorKey) {
        return theQRCodeGenerator.getOtpAuthURL(username, authenticatorKey);
    }

    @Override
    public QrCodeResponseDTO getCRCode(QrCodeRequestDTO request) throws GUSLErrorException {
        final UserSecurityDO userSecurity = HttpUtils.waitForFuture(theAuthClient.getUserSecurityByUser(SignInRequestDO.builder()
                .username(request.getUsername())
                .build()));
        return QrCodeResponseDTO.of(theQRCodeGenerator.getOtpAuthURL(userSecurity.getUsername(),
                ObfuscatedStorage.decryptKey(userSecurity.getAuthenticatorKey())));
    }

    public String generateSVG(UserDO user, UserSecurityDO userSecurity) throws GUSLErrorException {
        try {
            return theQRCodeGenerator.generateQRSVG(user, userSecurity);
        } catch (WriterException ex) {
            throw SystemErrors.IO_EXCEPTION.generateException(ex);
        }
    }

//    private String generateScratchCode() {
//
//    }
//    public ScratchCodesDO generateScratchCodes() throws GUSLErrorException {
//        List<ScratchCodeDO> codes = new ArrayList<>(NUMBER_SCRATCH_CODES);
//        IntStream.range(1, NUMBER_SCRATCH_CODES)
//                .forEach(idx -> {
//                    codes.add(ScratchCodeDO.builder()
//                                    .number()
//                                    .used(false)
//                            .build());
//                });
//
//        return ScratchCodesDO.builder().codes(codes).build();
//    }
}
