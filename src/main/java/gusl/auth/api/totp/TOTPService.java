package gusl.auth.api.totp;

import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.model.totp.QrCodeRequestDTO;
import gusl.auth.model.totp.QrCodeResponseDTO;
import gusl.auth.model.users.frontend.UserDO;
import gusl.core.exceptions.GUSLErrorException;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface TOTPService {

    UserSecurityDO generateUserKey(UserSecurityDO userSecurity);

    boolean validate(UserSecurityDO userSecurity, int code);

    String getCRCode(String username,String authenticatorKey);

    QrCodeResponseDTO getCRCode(QrCodeRequestDTO request) throws GUSLErrorException;

    String generateSVG(UserDO user, UserSecurityDO userSecurity) throws GUSLErrorException;
}
