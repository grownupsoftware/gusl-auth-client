package gusl.auth.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.auth.AuthType;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AuthTypesConfig {

    private List<AuthType> allowedAuthTypes;

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String defaultAuthType; // <== used by FE

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
