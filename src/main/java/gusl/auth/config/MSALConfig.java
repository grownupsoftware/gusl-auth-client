package gusl.auth.config;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

/**
 * @author dhudson
 * @since 02/08/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class MSALConfig {
    private String clientId;
    private String authority;
    private String secretKey;
    private String redirectUriSignin;
    private String redirectUriGraph;
    private String msGraphEndpointHost;
    private Set<String> scopes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
