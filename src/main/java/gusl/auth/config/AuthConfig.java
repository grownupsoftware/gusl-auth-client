package gusl.auth.config;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AuthConfig {

    private GoogleSSOConfig googleSSOConfig;
    private MSALConfig msalConfig;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
