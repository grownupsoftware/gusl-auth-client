// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package gusl.auth.msal;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class StateData {
    private String nonce;
    private Date expirationDate;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
