package gusl.auth.msal;

/**
 * @author dhudson
 * @since 02/08/2022
 */
public interface MSALConstants {

    String STATE = "state";
    String STATES = "states";
    Integer STATE_TTL = 3600;

    String FAILED_TO_VALIDATE_MESSAGE = "Failed to validate data received from Authorization service - ";

    String PRINCIPAL_SESSION_NAME = "principal";
    String TOKEN_CACHE_SESSION_ATTRIBUTE = "token_cache";

}
