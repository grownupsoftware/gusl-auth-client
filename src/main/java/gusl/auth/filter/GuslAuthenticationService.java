package gusl.auth.filter;

import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.core.exceptions.GUSLErrorException;
import org.jvnet.hk2.annotations.Contract;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.AsyncResponse;

@Contract
public interface GuslAuthenticationService {

    ILoggedInUser getLoggedInDetails(HttpServletRequest request) throws GUSLErrorException;

    void signIn(final AsyncResponse asyncResponse, HttpServletRequest servletRequest, SignInRequestDO request) throws GUSLErrorException;

    void tokenLogin(AsyncResponse asyncResponse, HttpServletRequest servletRequest, TokenLoginRequestDO request) throws GUSLErrorException;

    void signOut(AsyncResponse asyncResponse, LoggedInUserDO loggedInUser) throws GUSLErrorException;

}
