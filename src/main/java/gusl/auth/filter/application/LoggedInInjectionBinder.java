package gusl.auth.filter.application;

import gusl.auth.filter.AbstractLoggedInInjectionBinder;
import lombok.CustomLog;

import javax.inject.Singleton;

@Singleton
@CustomLog
public class LoggedInInjectionBinder extends AbstractLoggedInInjectionBinder {

    public LoggedInInjectionBinder() {
        super(UserLoggedInValueFactoryProvider.class);
    }
}
