/* Copyright lottomart */
package gusl.auth.filter.application;

import gusl.auth.filter.AbstractLoggedInValueFactoryProvider;
import gusl.auth.filter.GuslAuthenticationService;

import javax.inject.Inject;

public class UserLoggedInValueFactoryProvider extends AbstractLoggedInValueFactoryProvider {

    @Inject
    private GuslAuthenticationService theService;

    @Override
    public GuslAuthenticationService getAuthenticationService() {
        return theService;
    }

}
