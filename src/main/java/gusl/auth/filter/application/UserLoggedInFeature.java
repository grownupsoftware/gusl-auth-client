package gusl.auth.filter.application;

import lombok.CustomLog;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;

import javax.inject.Singleton;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

@Provider
@CustomLog
public class UserLoggedInFeature implements Feature {

    @Override
    public boolean configure(FeatureContext context) {
        logger.info("-- registering user logged in feature");
        context.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(UserLoggedInValueFactoryProvider.class)
                        .to(ValueParamProvider.class)
                        .in(Singleton.class);
            }
        });

        return true;
    }
}
