package gusl.auth.filter.application;

import gusl.node.bootstrap.GUSLServiceLocator;
import lombok.CustomLog;
import org.glassfish.jersey.server.internal.LocalizationMessages;
import org.glassfish.jersey.server.model.AnnotatedMethod;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import java.util.Arrays;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * RolesAllowed Dynamic Feature.
 * <p>
 * The Jersey one is, well, a bit poo.
 * <p>
 * It allows unauthed methods to be executed by default. What we want is that
 * unless a resource (method) has @PermitAll (class or method) or @RolesAllowed,
 * (class or method) it will be denied.
 * <p>
 * To use this security feature, register it with
 * register(GUSLRolesAllowedDynamicFeature.class); in the application.
 *
 * @author dhudson
 */
@CustomLog
public class GUSLRolesAllowedDynamicFeature implements DynamicFeature {

    @Override
    public void configure(final ResourceInfo resourceInfo, final FeatureContext configuration) {

        // logger.info("configure: {}", resourceInfo.getResourceClass().getName());

        // First things first, is it one of ours?
        if (!GUSLServiceLocator.hasCorrectPrefix(resourceInfo.getResourceClass().getName())) {
            // logger.debug("[{}] - Ignoring, not one of ours", resourceInfo.getResourceClass().getName());
            return;
        }

        final AnnotatedMethod am = new AnnotatedMethod(resourceInfo.getResourceMethod());

        // DenyAll on the method take precedence over RolesAllowed and PermitAll
        if (am.isAnnotationPresent(DenyAll.class)) {
            logger.debug("[{}] [{}] - Deny All", resourceInfo.getResourceClass().getName(), am.getMethod().getName());
            configuration.register(new RolesAllowedRequestFilter());
            return;
        }

        // RolesAllowed on the method takes precedence over PermitAll
        RolesAllowed ra = am.getAnnotation(RolesAllowed.class);
        if (ra != null) {
            logger.debug("[{}] [{}] - Roles allowed [{}]", resourceInfo.getResourceClass().getName(), am.getMethod().getName(), ra.value());
            configuration.register(new RolesAllowedRequestFilter(ra.value()));
            return;
        }

        // PermitAll takes precedence over RolesAllowed on the class
        if (am.isAnnotationPresent(PermitAll.class)) {
            // Do nothing.
            logger.debug("[{}] [{}] - Permit all", resourceInfo.getResourceClass().getName(), am.getMethod().getName());
            return;
        }

        // RolesAllowed on the class takes precedence over PermitAll
        ra = resourceInfo.getResourceClass().getAnnotation(RolesAllowed.class);
        if (ra != null) {
            logger.debug("[{}] - Roles allowed [{}]", resourceInfo.getResourceClass().getName(), ra.value());
            configuration.register(new RolesAllowedRequestFilter(ra.value()));
            return;
        }

        if (resourceInfo.getResourceClass().getAnnotation(PermitAll.class) != null) {
            // PermitAll at class level
            logger.debug("[{}] - Permit all", resourceInfo.getResourceClass().getName());
            return;
        }

        if (resourceInfo.getResourceClass().getAnnotation(DenyAll.class) != null) {
            logger.debug("[{}] - Deny All", resourceInfo.getResourceClass().getName());
            configuration.register(new RolesAllowedRequestFilter());
            return;
        }

        logger.warn("Unannotated resource method, access will be denied. {}:{}", resourceInfo.getResourceClass().getName(), am.getMethod().getName());
        // Reject the resource as it doesn't have an annotation
        configuration.register(new RolesAllowedRequestFilter());
    }

    @Priority(Priorities.AUTHORIZATION) // authorization filter - should go after any authentication filters
    private static class RolesAllowedRequestFilter implements ContainerRequestFilter {

        private final boolean denyAll;
        private final String[] rolesAllowed;

        RolesAllowedRequestFilter() {
            this.denyAll = true;
            this.rolesAllowed = null;
        }

        RolesAllowedRequestFilter(final String[] rolesAllowed) {
            this.denyAll = false;
            this.rolesAllowed = (rolesAllowed != null) ? rolesAllowed : new String[]{};
        }

        @Override
        public void filter(final ContainerRequestContext requestContext) {
            logger.debug(" {} {} denyAll:[{}] rolesAllowed:[{}]",
                    requestContext.getMethod(),
                    requestContext.getUriInfo().getAbsolutePath(),
                    denyAll,
                    rolesAllowed);

            if (!denyAll) {
                if (isNull(rolesAllowed) || rolesAllowed.length > 0 && !isAuthenticated(requestContext)) {
                    logger.debug("Forbidden Exception. Roles Allowed:[{}] authenticated:[{}]", isNull(rolesAllowed) ? "null" : Arrays.asList(rolesAllowed), isAuthenticated(requestContext));
                    throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
                }

                for (final String role : rolesAllowed) {
                    logger.debug("role: {} isUser in Role: {}", role, requestContext.getSecurityContext().isUserInRole(role));
                    if (requestContext.getSecurityContext().isUserInRole(role)) {
                        return;
                    }
                }
                logger.warn("Forbidden Exception. No role found. Roles Allowed:[{}] Path: [{}]",
                        Arrays.asList(rolesAllowed),
                        requestContext.getUriInfo().getAbsolutePath());

                log(requestContext);
            }

            throw new ForbiddenException(LocalizationMessages.USER_NOT_AUTHORIZED());
        }

        private void log(final ContainerRequestContext requestContext) {
            StringBuilder builder = new StringBuilder();
            builder.append("User: ").append(requestContext.getSecurityContext().getUserPrincipal().getName()).append("\n");
            builder.append("Path: ").append(requestContext.getUriInfo().getAbsolutePath()).append("\n");
            builder.append("Security: ").append(requestContext.getSecurityContext().getClass().getSimpleName()).append("\n");

            if (logger.isDebugEnabled()) {
                for (final String role : rolesAllowed) {
                    builder.append("\t").append(role).append(" isUserInRole: ").append(requestContext.getSecurityContext().isUserInRole(role)).append("\n");
                }
                logger.info("User not in role:\n{}", builder.toString());
            }

        }

        private static boolean isAuthenticated(final ContainerRequestContext requestContext) {

            logger.debug("isAuthenticated have for [{}] have context:[{}] have security:[{}] have principal:[{}]",
                    requestContext.getUriInfo().getPath(),
                    nonNull(requestContext),
                    nonNull(requestContext) && nonNull(requestContext.getSecurityContext()),
                    nonNull(requestContext) && nonNull(requestContext.getSecurityContext()) && nonNull(requestContext.getSecurityContext().getUserPrincipal()));

            if (isNull(requestContext.getSecurityContext())) {
                return false;
            }
            return nonNull(requestContext.getSecurityContext().getUserPrincipal());
        }
    }
}
