package gusl.auth.filter;

import lombok.CustomLog;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;

import javax.inject.Singleton;

@CustomLog
public abstract class AbstractLoggedInInjectionBinder extends AbstractBinder {

    private final Class<? extends ValueParamProvider> factoryProviderClass;

    public AbstractLoggedInInjectionBinder(Class<? extends ValueParamProvider> factoryProviderClass) {
        this.factoryProviderClass = factoryProviderClass;
    }

    @Override
    protected void configure() {
        logger.info("-- bind LoggedInInjector: {} ", factoryProviderClass.getCanonicalName());
        bind(factoryProviderClass).to(ValueParamProvider.class).in(Singleton.class);
    }
}
