package gusl.auth.filter;

import com.google.zxing.WriterException;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.authhelpers.AuthenticationHelper;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.model.totp.QRCodeGenerator;
import gusl.auth.model.totp.QrCodeRequestDTO;
import gusl.auth.model.totp.QrCodeResponseDTO;
import gusl.auth.model.users.frontend.UserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.security.ObfuscatedStorage;
import gusl.model.errors.SystemErrors;
import gusl.model.exceptions.ValidationException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractAuthenticationService {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    protected GoogleAuthenticator theAuthenticator;

    public AbstractAuthenticationService() {
        this.theAuthenticator = new GoogleAuthenticator();
    }

    @Inject
    private QRCodeGenerator theQRCodeGenerator;

    @Inject
    protected AuthenticationHelper theAuthenticationHelper;

    public abstract LoggedInUserCache getLoggedInCache();

    public ILoggedInUser getLoggedInDetails(HttpServletRequest request) throws GUSLErrorException {
        logger.debug("getLoggedInDetails: {}", request.getPathInfo());
        return theAuthenticationHelper.getLoggedInDetailsFor(getLoggedInCache(), request);
    }

    public SecurityContext getSecurityContextFor(Map<String, String> headers) throws ValidationException {
        return theAuthenticationHelper.getSecurityContextFor(getLoggedInCache(), headers);
    }

    public SecurityContext getSecurityContextFor(ContainerRequestContext requestContext) throws ValidationException {
        return theAuthenticationHelper.getSecurityContextFor(getLoggedInCache(), requestContext);
    }

    public UserSecurityDO generateUserKey(UserSecurityDO userSecurity) {
        GoogleAuthenticatorKey key = theAuthenticator.createCredentials();
        userSecurity.setAuthenticatorKey(key.getKey());
        return userSecurity;
    }

    public boolean validate(UserSecurityDO userSecurity, int code) {
        String secretKey = ObfuscatedStorage.isObfuscated(userSecurity.getAuthenticatorKey())
                ? ObfuscatedStorage.decryptKey(userSecurity.getAuthenticatorKey())
                : userSecurity.getAuthenticatorKey();
        return theAuthenticator.authorize(secretKey, code);
    }

    public String getCRCode(String username, String authenticatorKey) {
        return theQRCodeGenerator.getOtpAuthURL(username, authenticatorKey);
    }

    private String getCRCode(UserSecurityDO userSecurity) {
        return theQRCodeGenerator.getOtpAuthURL(userSecurity.getUsername(), userSecurity.getAuthenticatorKey());
    }

    public abstract Optional<UserSecurityDO> getUserByUsername(String username) throws GUSLErrorException;

    public QrCodeResponseDTO getCRCode(QrCodeRequestDTO request) throws GUSLErrorException {
        return QrCodeResponseDTO.of(getCRCode(getUserByUsername(request.getUsername())
                .orElseThrow(() -> AuthenticationErrors.USERNAME_NOT_FOUND.generateException(request.getUsername()))));
    }

    public String generateSVG(UserDO user, UserSecurityDO userSecurity) throws GUSLErrorException {
        try {
            return theQRCodeGenerator.generateQRSVG(user, userSecurity);
        } catch (WriterException ex) {
            throw SystemErrors.IO_EXCEPTION.generateException(ex);
        }
    }

}
