package gusl.auth.filter;

import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.loggedin.LoggedInUser;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.node.converter.ModelConverter;
import lombok.SneakyThrows;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.model.Parameter;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import java.util.function.Function;

public abstract class AbstractLoggedInValueFactoryProvider implements ValueParamProvider {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    private ModelConverter theModelConverter;

    @Context
    private ResourceContext context;

    public abstract GuslAuthenticationService getAuthenticationService();

    public <S, D> D convert(S sourceObject, Class<D> destinationClass) {
        return theModelConverter.convert(sourceObject, destinationClass);
    }

    @Override
    public Function<ContainerRequest, ?> getValueProvider(Parameter parameter) {
        if (parameter.getRawType().equals(LoggedInUserDO.class) && parameter.isAnnotationPresent(LoggedInUser.class)) {
            return new UserParamProvider();
        }
        return null;
    }

    private class UserParamProvider implements Function<ContainerRequest, LoggedInUserDO> {
        @SneakyThrows
        @Override
        public LoggedInUserDO apply(ContainerRequest containerRequest) {
            ILoggedInUser loggedInUser = getAuthenticationService().getLoggedInDetails(context.getResource(HttpServletRequest.class));
            logger.debug("Logged in  user {}", loggedInUser);
            return convert(loggedInUser, LoggedInUserDO.class);
        }
    }

    @Override
    public PriorityType getPriority() {
        return Priority.HIGH;
    }
}
