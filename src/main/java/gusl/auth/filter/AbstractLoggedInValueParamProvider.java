package gusl.auth.filter;

import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.loggedin.LoggedInUser;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import lombok.SneakyThrows;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.model.Parameter;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import java.util.function.Function;

public abstract class AbstractLoggedInValueParamProvider<T extends ILoggedInUser> implements ValueParamProvider {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Context
    private ResourceContext context;

    @Inject
    private GuslAuthenticationService theService;

    @Override
    public Function<ContainerRequest, ?> getValueProvider(Parameter parameter) {
        try {
            // logger.info("============= parameter: {} {} {} {} {}",parameter.getSourceName(), parameter, parameter.getRawType(),parameter.getRawType().equals(LoggedInUserDO.class),parameter.isAnnotationPresent(LoggedInUser.class));
            if (parameter.getRawType().equals(LoggedInUserDO.class)) {
                logger.info("++++++++++++++ parameter: {} {} ", parameter.getSourceName(), parameter.getRawType());
            }
            if (parameter.getRawType().equals(LoggedInUserDO.class)
                    && parameter.isAnnotationPresent(LoggedInUser.class)) {
                logger.info("============= parameter: {} {} ", parameter.getSourceName(), parameter.getRawType());
                return new UserParamProvider();
            }
//            if (parameter.getRawType().equals(AdminLoggedInUserDO.class)) {
//                logger.info("=======xxx====== parameter: {} {} ", parameter.getSourceName(), parameter.getRawType());
//                return new UserParamProvider();
//            }

        } catch (Throwable t) {
            logger.error("error", t);
        }
        return null;
    }

    private class UserParamProvider implements Function<ContainerRequest, ILoggedInUser> {
        @SneakyThrows
        @Override
        public ILoggedInUser apply(ContainerRequest containerRequest) {
            logger.info("Logging in using LoggedInValueParamProvider");

            final HttpServletRequest request = context.getResource(HttpServletRequest.class);
            ILoggedInUser loggedInUser = theService.getLoggedInDetails(request);
            logger.info("Logged in  user {}", loggedInUser);
            return loggedInUser;
        }
    }

    @Override
    public PriorityType getPriority() {
        return Priority.HIGH;
    }
}
