package gusl.auth.filter.authhelpers;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import gusl.auth.AuthType;
import gusl.auth.config.AuthConfig;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.model.session.Session;
import gusl.core.security.ObfuscatedStorage;
import gusl.model.exceptions.AccessDeniedException;
import gusl.model.exceptions.TotpNotConfiguredException;
import gusl.model.exceptions.UnAuthorizedException;

import javax.inject.Singleton;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.util.Objects.nonNull;

@Singleton
public class TimedOTPHelper extends UsernamePasswordHelper implements AuthHelper {
    private static final String USER_TOKEN_NAME = "totp-user-token";
    private static final String SESSION_TOKEN_NAME = "totp-session-token";

    private GoogleAuthenticator theAuthenticator;

    public AuthType getAuthType() {
        return AuthType.TIME_BASED_ONE_TIME_PASSWORD;
    }

    @Override
    public int getPriority() {
        return 30;
    }

    @Override
    protected String getUserTokenName() {
        return USER_TOKEN_NAME;
    }

    @Override
    protected String getSessionTokenName() {
        return SESSION_TOKEN_NAME;
    }

    @Override
    public boolean verifySignIn(LoggedInUserCache loggedInCache, UserSecurityDO userSecurity, SignInRequestDO request) {
        String secretKey = ObfuscatedStorage.isObfuscated(userSecurity.getAuthenticatorKey())
                ? ObfuscatedStorage.decryptKey(userSecurity.getAuthenticatorKey())
                : userSecurity.getAuthenticatorKey();
        final boolean authorized = nonNull(request.getTotp()) && theAuthenticator.authorize(secretKey, Integer.parseInt(request.getTotp()));
        if (!authorized) {
            logger.audit("SignIn {} - OTP validation failed [{}]", userSecurity.getUsername(), (nonNull(request.getTotp()) ? Integer.parseInt(request.getTotp()) : "null"));
        }
        return authorized;
    }

    @Override
    public void initialise(AuthConfig authConfig) {
        scheduleCleanUp();
        theAuthenticator = new GoogleAuthenticator();
    }

    @Override
    public void validate(String userId, ILoggedInUser user, Session session, String uri) {
        if (!user.canLogin()) {
            logger.warn("User {} is not allowed to login", user.getId());
            throw new AccessDeniedException(AuthenticationErrors.CUSTOMER_DENIED.getError());
        }
        if (!session.getStatus().isActive()) {
            logger.warn("Invalid status on session token {} {} ", userId, session.getStatus());
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        // change-password fails
        if (nonNull(uri) && uri.contains("change-password")) {
            logger.debug("No check for TOTP for change password", uri);
            return;
        }

        final ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        boolean passwordChangeRequired = nonNull(user.getNextPasswordChange()) && user.getNextPasswordChange().isBefore(now);
        if (passwordChangeRequired) {
            logger.warn("Password change required {}", userId);
            throw new TotpNotConfiguredException(AuthenticationErrors.PASSWORD_CHANGE_REQUIRED.getError());
        }

        if (Boolean.TRUE.equals(session.getPreSignIn())) {
            logger.warn("Totp entry required {}", userId);
            throw new TotpNotConfiguredException(AuthenticationErrors.TOTP_ENTRY_REQUIRED.getError());
        }

        if (user.isTOTPRequired() && !user.isUserRegisteredForTOTP()) {
            logger.warn("Totp required but user not registered yet {}", userId);
            throw new TotpNotConfiguredException(AuthenticationErrors.TOTP_SETUP_REQUIRED.getError());
        }
    }

}
