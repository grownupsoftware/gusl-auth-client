package gusl.auth.filter.authhelpers;

import gusl.auth.AuthType;
import gusl.auth.config.AuthConfig;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.security.UserSecurityDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.exceptions.UnAuthorizedException;
import gusl.node.bootstrap.GUSLServiceLocator;
import lombok.CustomLog;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Singleton
@CustomLog
public class AuthenticationHelper {

    private final Map<AuthType, AuthHelper> theAuthHelpers = new ConcurrentHashMap<>();

    AuthConfig theAuthConfig;
    private boolean initialised = false;

    public void initialise(AuthConfig authConfig) {
        theAuthConfig = authConfig;
        initialiseHelpers();
    }

    private void initialiseHelpers() {
        if (theAuthHelpers.isEmpty()) {
            register(GoogleSSOHelper.class);
            register(TimedOTPHelper.class);
            register(UsernamePasswordHelper.class);
            register(MSALSSOHelper.class);
        }

        if (!initialised && !theAuthHelpers.isEmpty()) {
            initialised = true;
            safeStream(theAuthHelpers.values()).forEach(helper -> helper.initialise(theAuthConfig));
        }
    }

    private void register(Class<? extends AuthHelper> klass) {
        final AuthHelper authHelper = GUSLServiceLocator.getInstanceOf(klass);
        logger.debug("Registered: {} {}", authHelper.getAuthType(), klass.getName());
        theAuthHelpers.put(authHelper.getAuthType(), authHelper);
    }

    public AuthType ascertainAuthType(LoggedInUserCache loggedInCache, HttpServletRequest request) {
        if (!initialised) {
            logger.error("AuthenticationHelper not initialised - ensure initialise(AuthConfig) called from your security service");
        }
        final Optional<AuthType> first = safeStream(theAuthHelpers.values())
                .filter(authHelper -> loggedInCache.getAuthTypes().contains(authHelper.getAuthType()))
                .sorted(Comparator.comparing(AuthHelper::getPriority))
                .map(authHelper -> authHelper.canAuthRequest(request) ? authHelper.getAuthType() : null
                ).filter(Objects::nonNull)
                .findFirst();
        if (first.isEmpty()) {
            logger.warn("Failed to identify auth type from HttpServletRequest: {}", request);
            throw new UnAuthorizedException(AuthenticationErrors.UNAUTHORIZED.getError());
        }
        logger.debug("{} HttpServletRequest Auth using: {}", request.getRequestURI(), first.get());

        return first.get();
    }

    public AuthType ascertainAuthType(LoggedInUserCache loggedInCache, ContainerRequestContext request) {
        final Optional<AuthType> first = safeStream(theAuthHelpers.values())
                .filter(authHelper -> loggedInCache.getAuthTypes().contains(authHelper.getAuthType()))
                .sorted(Comparator.comparing(AuthHelper::getPriority))
                .map(authHelper -> authHelper.canAuthRequest(request) ? authHelper.getAuthType() : null
                ).filter(Objects::nonNull)
                .findFirst();
        if (first.isEmpty()) {
            return null;
        }
        logger.debug("{} RequestContext Auth using: {}", request.getUriInfo().getPath(), first.get());
        return first.get();
    }

    public AuthType ascertainAuthType(LoggedInUserCache loggedInCache, Map<String, String> headers) {
        final Optional<AuthType> first = safeStream(theAuthHelpers.values())
                .filter(authHelper -> loggedInCache.getAuthTypes().contains(authHelper.getAuthType()))
                .sorted(Comparator.comparing(AuthHelper::getPriority))
                .map(authHelper -> authHelper.canAuthRequest(headers) ? authHelper.getAuthType() : null
                ).filter(Objects::nonNull)
                .findFirst();
        if (first.isEmpty()) {
            logger.warn("Failed to identify auth type from headers: {}", headers);
            throw new UnAuthorizedException(AuthenticationErrors.UNAUTHORIZED.getError());
        }
        logger.debug("headers Auth using: {}", first.get());
        return first.get();
    }

    public AuthType ascertainAuthType(LoggedInUserCache loggedInCache, TokenLoginRequestDO request) {
        final Optional<AuthType> first = safeStream(theAuthHelpers.values())
                .filter(authHelper -> loggedInCache.getAuthTypes().contains(authHelper.getAuthType()))
                .sorted(Comparator.comparing(AuthHelper::getPriority))
                .map(authHelper -> authHelper.canAuthRequest(request) ? authHelper.getAuthType() : null
                ).filter(Objects::nonNull)
                .findFirst();
        if (first.isEmpty()) {
            logger.warn("Failed to identify auth type from token:[{}] user:[{}] session:[{}]",
                    trimToken(request.getToken()),
                    trimToken(request.getUserToken()),
                    trimToken(request.getSessionToken()));
            throw new UnAuthorizedException(AuthenticationErrors.UNAUTHORIZED.getError());
        }
        logger.debug("token-login Auth using: {}", first.get());
        return first.get();
    }

    public ILoggedInUser getLoggedInDetailsFor(LoggedInUserCache loggedInCache, HttpServletRequest request) throws GUSLErrorException {
        AuthType type = ascertainAuthType(loggedInCache, request);
        return isNull(type) ? null : theAuthHelpers.get(type).getLoggedInDetailsFor(loggedInCache, request);
    }

    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, Map<String, String> headers) {
        AuthType type = ascertainAuthType(loggedInCache, headers);
        return isNull(type) ? null : theAuthHelpers.get(type).getSecurityContextFor(loggedInCache, headers);
    }

    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, ContainerRequestContext request) {
        AuthType type = ascertainAuthType(loggedInCache, request);
        return isNull(type) ? null : theAuthHelpers.get(type).getSecurityContextFor(loggedInCache, request);
    }

    public TokenLoginResponseDO tokenLogin(LoggedInUserCache loggedInCache, TokenLoginRequestDO request) throws GUSLErrorException {
        AuthType type = ascertainAuthType(loggedInCache, request);
        return isNull(type) ? null : theAuthHelpers.get(type).tokenLogin(loggedInCache, request);
    }

    private String trimToken(String token) {
        if (isNull(token)) {
            return "";
        }
        if (token.length() < 10) {
            return token;
        }
        return token.substring(0, 5) + "..." + token.substring(token.length() - 5);
    }

    public boolean verifySignIn(LoggedInUserCache loggedInCache, UserSecurityDO userSecurity, SignInRequestDO request) {
        if (nonNull(request) && nonNull(request.getTotp())) {
            return theAuthHelpers.get(AuthType.TIME_BASED_ONE_TIME_PASSWORD).verifySignIn(loggedInCache, userSecurity, request);
        }
        return true;
    }

}
