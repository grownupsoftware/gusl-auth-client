package gusl.auth.filter.authhelpers;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import gusl.annotations.form.MediaType;
import gusl.auth.AuthType;
import gusl.auth.cache.role.RoleCache;
import gusl.auth.client.AuthClient;
import gusl.auth.config.AuthConfig;
import gusl.auth.config.MSALConfig;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.filter.model.MsalSSOUserDO;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.roles.RoleHelper;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.roles.RoleBasedSecurityContext;
import gusl.auth.roles.SuperUserSecurityContext;
import gusl.core.dates.DateUtils;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.JsonUtils;
import gusl.core.utils.StringUtils;
import gusl.model.exceptions.UnAuthorizedException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static gusl.core.utils.StringUtils.isBlank;
import static gusl.core.utils.StringUtils.isNotBlank;
import static gusl.node.transport.HttpUtils.waitForFuture;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author dhudson
 * @since 02/08/2022
 */
@Singleton
public class MSALSSOHelper extends AbstractAuthHelper implements AuthHelper {

    public static final String MSAL_SSO_TOKEN_NAME = "msal-token";

    private final Map<String, MsalSSOUserDO> theLoggedInUserMap = new ConcurrentHashMap<>();

    private JwkProvider theJwkProvider = null;

    private MSALConfig theMSALConfig;

    @Inject
    private AuthClient theAuthClient;

    @Inject
    private RoleCache theRoleCache;

    @Override
    public AuthType getAuthType() {
        return AuthType.MSAL;
    }

    @Override
    public int getPriority() {
        return 5;
    }

    @Override
    public boolean canAuthRequest(HttpServletRequest request) {
        logger.debug("{} canAuthRequest: {}",
                request.getRequestURI(),
                isNotBlank(request.getHeader(MSAL_SSO_TOKEN_NAME)));
        return isNotBlank(request.getHeader(MSAL_SSO_TOKEN_NAME));
    }

    @Override
    public boolean canAuthRequest(ContainerRequestContext request) {
        logger.debug("{} canAuthRequest: {}",
                request.getUriInfo().getPath(),
                isNotBlank(request.getHeaderString(MSAL_SSO_TOKEN_NAME)));
        return isNotBlank(request.getHeaderString(MSAL_SSO_TOKEN_NAME));
    }

    @Override
    public boolean canAuthRequest(TokenLoginRequestDO request) {
        return nonNull(request.getMsalToken()) && nonNull(request.getMsalUsername());
    }

    @Override
    public boolean canAuthRequest(Map<String, String> headers) {
        return isNotBlank(headers.get(MSAL_SSO_TOKEN_NAME));
    }

    @Override
    public ILoggedInUser getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            HttpServletRequest request
    ) throws GUSLErrorException {
        String token = request.getHeader(MSAL_SSO_TOKEN_NAME);
        String mediaType = request.getHeader(MEDIA_TYPE_HEADER);
        String orientation = request.getHeader(ORIENTATION_HEADER);
        if (isBlank(token)) {
            logger.warn("MSAL SSO token in header is blank");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        return theLoggedInUserMap.containsKey(token)
                ? validateExistingToken(null, token, null, null, mediaType, orientation)
                : validateNewToken(null, token, null, null, mediaType, orientation);
    }

    @Override
    public void initialise(AuthConfig authConfig) {
        logger.info("authConfig: {}", JsonUtils.prettyPrint(authConfig));
        if (nonNull(authConfig)) {
            theMSALConfig = authConfig.getMsalConfig();
        }
        initialiseJwkProvider();
    }

    private void initialiseJwkProvider() {
        if (nonNull(theJwkProvider)) {
            return;
        }

        try {
            theJwkProvider = new UrlJwkProvider(new URL("https://login.microsoftonline.com/common/discovery/v2.0/keys"));
        } catch (Throwable t) {
            logger.error("Error configuring MSAL JWT verification - this will prevent MSAL login", t);
        }

    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, ContainerRequestContext request) {
        return getLoggedInDetailsFor(loggedInCache, request.getHeaderString(MSAL_SSO_TOKEN_NAME), request.getHeaderString(MEDIA_TYPE_HEADER), request.getHeaderString(ORIENTATION_HEADER));
    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, Map<String, String> headers) {
        return getLoggedInDetailsFor(loggedInCache, headers.get(MSAL_SSO_TOKEN_NAME), headers.get(MEDIA_TYPE_HEADER), headers.get(ORIENTATION_HEADER));
    }

    private SecurityContext getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            String token,
            String mediaType,
            String orientation
    ) {
        final MsalSSOUserDO ssoUser = theLoggedInUserMap.get(token);
        return isNull(ssoUser) ? null : ssoUser.getSecurityContext();
    }

    @Override
    public TokenLoginResponseDO tokenLogin(LoggedInUserCache loggedInCache, TokenLoginRequestDO request) throws GUSLErrorException {
        ILoggedInUser loggedInUser = theLoggedInUserMap.containsKey(request.getMsalToken())
                ? validateExistingToken(request.getMsalUsername(), request.getMsalToken(), request.getMsalName(), request.getTimezone(), null, null)
                : validateNewToken(request.getMsalUsername(), request.getMsalToken(), request.getMsalName(), request.getTimezone(), null, null);
        return TokenLoginResponseDO.builder()
                .id(loggedInUser.getId())
                .sessionToken(request.getMsalToken())
                .username(loggedInUser.getUsername())
                .avatar(loggedInUser.getAvatar())
                .build();
    }

    private ILoggedInUser validateExistingToken(
            String token,
            String timezone
    ) throws GUSLErrorException {
        final String username = verifyToken(token, timezone);
        final MsalSSOUserDO ssoUser = theLoggedInUserMap.get(token);
        if (isNull(ssoUser)) {
            logger.error("User not found in cache");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        if (!ssoUser.getUsername().equals(username)) {
            logger.error("Username in token does not match {} {}", ssoUser.getUsername(), username);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        return addToCache(token, ssoUser.getUsername(), ssoUser.getName(), null, null);
    }

    private ILoggedInUser validateExistingToken(
            String username,
            String token,
            String name,
            String timezone,
            String mediaType,
            String orientation
    ) throws GUSLErrorException {
        return validateNewToken(username, token, name, timezone, mediaType, orientation);
    }

    private ILoggedInUser validateNewToken(
            String loginUsername,
            String token,
            String name,
            String timezone,
            String mediaType,
            String orientation
    ) throws GUSLErrorException {
        String username = verifyToken(token, timezone);
        logger.debug("===================== validateNewToken username: {} size:{}", username, theLoggedInUserMap.size());
        if (nonNull(loginUsername) && !loginUsername.equals(username)) {
            logger.error("Username in token does not match {} {}", loginUsername, username);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }

        return addToCache(token, username, name, mediaType, orientation);
    }

    private ILoggedInUser addToCache(
            String token,
            String username,
            String name,
            String mediaTypeHeader,
            String orientationHeader
    ) throws GUSLErrorException {

        final UserSecurityDO userSecurity = waitForFuture(theAuthClient
                .getUserSecurityByUsername(GetUserByUsernameRequestDO.of(username)));

        MediaType mediaType = MediaType.Desktop;
        if (StringUtils.isNotBlank(mediaTypeHeader)) {
            try {
                mediaType = MediaType.valueOf(mediaTypeHeader);
            } catch (IllegalArgumentException e) {
            }
        }

        String orientation = null;
        if (StringUtils.isNotBlank(orientationHeader)) {
            orientation = orientationHeader;
        }

        LoggedInUserDO loggedInUser = LoggedInUserDO.builder()
                .id(userSecurity.getId())
                .username(username)
                .nickname(name)
                .avatar(null)
                .permissions(theRoleCache.getPermissionsForRoles(userSecurity.getRoleIds()))
                .roleIds(userSecurity.getRoleIds())
                .superUser(theRoleCache.isSuperUser(userSecurity.getRoleIds()))
                .authType(AuthType.MSAL)
                .allowedCohorts(userSecurity.getAllowedCohorts())
                .currentCohortId(getCurrentCohort(userSecurity.getAllowedCohorts()))
                .currentMediaType(mediaType)
                .orientation(orientation)
                .build();

        final SecurityContext securityContext = RoleHelper.isSuperUser(loggedInUser)
                ? new SuperUserSecurityContext(loggedInUser.getUsername())
                : new RoleBasedSecurityContext(loggedInUser.getUsername(), loggedInUser.getPermissions());

        theLoggedInUserMap.put(token,
                MsalSSOUserDO.builder()
                        .username(username)
                        .name(name)
                        .securityContext(securityContext)
                        .build());

        return loggedInUser;
    }

    public String verifyToken(String token, String timezone) throws GUSLErrorException {

        try {
            if (isNull(token)) {
                logger.warn("Null token");
                throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
            }
            DecodedJWT jwt = JWT.decode(token);
            Jwk jwk = theJwkProvider.get(jwt.getKeyId());
            Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
            algorithm.verify(jwt);// if the token signature is invalid, the method will throw SignatureVerificationException

            logger.debug("jwt {}", JsonUtils.prettyPrint(jwt));

            if (isNull(jwt.getAudience()) || !jwt.getAudience().contains(theMSALConfig.getClientId())) {
                logger.error("Audience does not match our client id", theMSALConfig.getClientId(), jwt.getAudience());
                throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
            }

            // use "exp" as UTC date
            final LocalDateTime expiryTime = getDateFromClaim(jwt, "exp");

            LocalDateTime timezoneTime = null;
            if (StringUtils.isNotBlank(timezone)) {
                try {
                    timezoneTime = LocalDateTime.now(ZoneId.of(timezone));
                    logger.debug("Now is {} for {}", timezoneTime, timezone);
                } catch (Throwable t) {
                    logger.warn("Failed to get date based on time zone: {}", timezone, t);
                }
            }
            final LocalDateTime now = nonNull(timezoneTime) ? timezoneTime : LocalDateTime.now(ZoneId.of("UTC"));

            if (now.isAfter(expiryTime)) {
                logger.error("Token [{}] has expired (now) {} after (token) {}", shorten(token), now, expiryTime);
                throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
            }

            String email = getStringFromClaim(jwt, "preferred_username");
            return email;
        } catch (Throwable t) {
            if (t instanceof UnAuthorizedException) {
                throw (UnAuthorizedException) t;
            }
            logger.error("Error validating MSAL JWT token", t);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }

    }

    private String shorten(String token) {
        if (StringUtils.isBlank(token) || token.length() < 10) {
            return token;
        }
        return token.substring(0, 5) + "..." + token.substring(token.length() - 5);
    }

    private LocalDateTime getDateFromClaim(DecodedJWT jwt, String claimName) throws GUSLErrorException {
        final Claim claim = jwt.getClaim(claimName);
        if (isNull(claim)) {
            logger.warn("Claim not found for [{}]", claimName);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        try {
            return DateUtils.asLocalDateTime(claim.asDate());
        } catch (Throwable t) {
            logger.warn("Claim has invalid date [{}]", claim.asDate());
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
    }

    private String getStringFromClaim(DecodedJWT jwt, String claimName) throws GUSLErrorException {
        final Claim claim = jwt.getClaim(claimName);
        if (isNull(claim)) {
            logger.warn("Claim not found for [{}]", claimName);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        try {
            return claim.asString();
        } catch (Throwable t) {
            logger.warn("Error getting claim for [{}]", claimName);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
    }

}

///   MSAL Stuff ...
/*
    public void processAuthenticationCodeRedirect(HttpServletRequest httpRequest, String currentUri, String fullUrl)
            throws Throwable {

        Map<String, List<String>> params = new HashMap<>();
        for (String key : httpRequest.getParameterMap().keySet()) {
            params.put(key, Collections.singletonList(httpRequest.getParameterMap().get(key)[0]));
        }
        // validate that state in response equals to state in request
        StateData stateData = SessionManagementHelper.validateState(httpRequest.getSession(),
                params.get(MSALConstants.STATE).get(0));

        AuthenticationResponse authResponse = AuthenticationResponseParser.parse(new URI(fullUrl), params);
        if (MSALUtils.isAuthenticationSuccessful(authResponse)) {
            AuthenticationSuccessResponse oidcResponse = (AuthenticationSuccessResponse) authResponse;
            // validate that OIDC Auth Response matches Code Flow (contains only requested artifacts)
            MSALUtils.validateAuthRespMatchesAuthCodeFlow(oidcResponse);

            IAuthenticationResult result = getAuthResultByAuthCode(
                    httpRequest,
                    oidcResponse.getAuthorizationCode(),
                    currentUri);

            // validate nonce to prevent reply attacks (code maybe substituted to one with broader access)
            validateNonce(stateData, getNonceClaimValueFromIdToken(result.idToken()));

            SessionManagementHelper.setSessionPrincipal(httpRequest, result);
        } else {
            AuthenticationErrorResponse oidcResponse = (AuthenticationErrorResponse) authResponse;
            throw new Exception(String.format("Request for auth code failed: %s - %s",
                    oidcResponse.getErrorObject().getCode(),
                    oidcResponse.getErrorObject().getDescription()));
        }
    }

    private IAuthenticationResult getAuthResultByAuthCode(HttpServletRequest httpServletRequest, AuthorizationCode authorizationCode,
                                                          String currentUri) throws Throwable {

        IAuthenticationResult result;
        try {
            String authCode = authorizationCode.getValue();
            AuthorizationCodeParameters parameters = AuthorizationCodeParameters.builder(authCode, new URI(currentUri)).build();

            Future<IAuthenticationResult> future = theMSALApplication.acquireToken(parameters);

            result = future.get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }

        if (result == null) {
            throw new ServiceUnavailableException("authentication result was null");
        }

        SessionManagementHelper.storeTokenCacheInSession(httpServletRequest, theMSALApplication.tokenCache().serialize());
        return result;
    }

    @SneakyThrows

    public ConfidentialClientApplication createClientApplication() {
        return ConfidentialClientApplication.builder(theConfig.getClientId(),
                        ClientCredentialFactory.createFromSecret(theConfig.getSecretKey()))
                .authority(theConfig.getAuthority()).build();
    }

    public IAuthenticationResult getAuthResultBySilentFlow(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws Throwable {

        IAuthenticationResult result = MSALUtils.getAuthSessionObject(httpRequest);
        Object tokenCache = httpRequest.getSession().getAttribute("token_cache");
        if (tokenCache != null) {
            theMSALApplication.tokenCache().deserialize(tokenCache.toString());
        }

        SilentParameters parameters = SilentParameters.builder(Collections.singleton("User.Read"), result.account()).build();

        CompletableFuture<IAuthenticationResult> future = theMSALApplication.acquireTokenSilently(parameters);
        IAuthenticationResult updatedResult = future.get();

        //update session with latest token cache
        SessionManagementHelper.storeTokenCacheInSession(httpRequest, theMSALApplication.tokenCache().serialize());

        return updatedResult;
    }

    void sendAuthRedirect(HttpServletRequest httpRequest, HttpServletResponse httpResponse, String scope, String redirectURL)
            throws IOException {

        // state parameter to validate response from Authorization server and nonce parameter to validate idToken
        String state = UUID.randomUUID().toString();
        String nonce = UUID.randomUUID().toString();

        SessionManagementHelper.storeStateAndNonceInSession(httpRequest.getSession(), state, nonce);

        httpResponse.setStatus(302);
        String authorizationCodeUrl = getAuthorizationCodeUrl(httpRequest.getParameter("claims"), scope, redirectURL, state, nonce);
        httpResponse.sendRedirect(authorizationCodeUrl);
    }

    public String getAuthorizationCodeUrl(String claims, String scope, String registeredRedirectURL,
                                          String state, String nonce)
            throws MalformedURLException {

        String updatedScopes = scope == null ? "" : scope;

        PublicClientApplication pca =
                PublicClientApplication.builder(theConfig.getClientId()).authority(theConfig.getAuthority()).build();

        AuthorizationRequestUrlParameters parameters =
                AuthorizationRequestUrlParameters
                        .builder(registeredRedirectURL,
                                Collections.singleton(updatedScopes))
                        .responseMode(ResponseMode.QUERY)
                        .prompt(Prompt.SELECT_ACCOUNT)
                        .state(state)
                        .nonce(nonce)
                        .claimsChallenge(claims)
                        .build();

        return pca.getAuthorizationRequestUrl(parameters).toString();
    }
 */
