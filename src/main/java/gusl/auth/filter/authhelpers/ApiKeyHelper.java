package gusl.auth.filter.authhelpers;

import gusl.auth.AuthType;
import gusl.auth.config.AuthConfig;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.security.UserSecurityDO;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;

import static gusl.core.utils.StringUtils.isNotBlank;
import static java.util.Objects.nonNull;

@Singleton
public class ApiKeyHelper extends AbstractAuthHelper implements AuthHelper {

    public static final String API_ID = "api_id";
    public static final String API_PASSWORD = "api_password";

    @Override
    public AuthType getAuthType() {
        return AuthType.API_KEY;
    }

    @Override
    public int getPriority() {
        return 10;
    }

    @Override
    public boolean canAuthRequest(HttpServletRequest request) {
        logger.info("{} canAuthRequest: {}",
                request.getRequestURI(),
                isNotBlank(request.getHeader(API_ID))
                        && isNotBlank(request.getHeader(API_PASSWORD)));
        return isNotBlank(request.getHeader(API_ID)) && isNotBlank(request.getHeader(API_PASSWORD));
    }

    @Override
    public boolean canAuthRequest(ContainerRequestContext request) {
        logger.info("{} canAuthRequest: {}",
                request.getUriInfo().getPath(),
                isNotBlank(request.getHeaderString(API_ID))
                        && isNotBlank(request.getHeaderString(API_PASSWORD)));
        return isNotBlank(request.getHeaderString(API_ID)) && isNotBlank(request.getHeaderString(API_PASSWORD));
    }

    @Override
    public boolean canAuthRequest(Map<String, String> headers) {
        return isNotBlank(headers.get(API_ID)) && isNotBlank(headers.get(API_PASSWORD));
    }

    @Override
    public boolean canAuthRequest(TokenLoginRequestDO request) {
        return nonNull(request.getApiId()) && nonNull(request.getApiPassword());
    }

    @Override
    public ILoggedInUser getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            HttpServletRequest request
    ) {
        return null;
    }

    @Override
    public void initialise(AuthConfig authConfig) {

    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, ContainerRequestContext request) {
        return null;
    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, Map<String, String> headers) {
        return null;
    }

    @Override
    public TokenLoginResponseDO tokenLogin(LoggedInUserCache loggedInCache, TokenLoginRequestDO request) {
        // not implemented
        return null;
    }



}
