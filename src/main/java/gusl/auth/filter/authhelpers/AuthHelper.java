package gusl.auth.filter.authhelpers;

import gusl.auth.AuthType;
import gusl.auth.config.AuthConfig;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.jwt.JwtService;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.security.UserSecurityDO;
import gusl.core.exceptions.GUSLErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;

public interface AuthHelper {

    String MEDIA_TYPE_HEADER = "Media-Type";

    String ORIENTATION_HEADER = "Orientation";

    AuthType getAuthType();

    int getPriority(); // if multiple auth types supported, lowest number is highest priority

    boolean canAuthRequest(HttpServletRequest request);

    boolean canAuthRequest(ContainerRequestContext requestContext);

    boolean canAuthRequest(Map<String, String> headers);

    boolean canAuthRequest(TokenLoginRequestDO request);

    ILoggedInUser getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            HttpServletRequest request) throws GUSLErrorException;

    void initialise(AuthConfig authConfig);

    SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, ContainerRequestContext request);

    SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, Map<String, String> headers);

    TokenLoginResponseDO tokenLogin(LoggedInUserCache loggedInCache, TokenLoginRequestDO request) throws GUSLErrorException;

    boolean verifySignIn(LoggedInUserCache loggedInCache, UserSecurityDO userSecurity, SignInRequestDO request);
}
