package gusl.auth.filter.authhelpers;

import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.security.UserSecurityDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.model.cohort.CohortIdsDO;

import static java.util.Objects.isNull;

public abstract class AbstractAuthHelper {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    public boolean verifySignIn(LoggedInUserCache loggedInCache, UserSecurityDO userSecurity, SignInRequestDO request) {
        // used by TimedOTP to verify google auth code
        return true;
    }

    protected String getCurrentCohort(CohortIdsDO allowedCohorts) {
        if (isNull(allowedCohorts) || isNull(allowedCohorts.getIds()) || allowedCohorts.getIds().isEmpty()) {
            return null;
        }
        return allowedCohorts.getIds().get(0);
    }
}
