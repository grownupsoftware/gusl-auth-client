package gusl.auth.filter.authhelpers;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import gusl.annotations.form.MediaType;
import gusl.auth.AuthType;
import gusl.auth.cache.role.RoleCache;
import gusl.auth.client.AuthClient;
import gusl.auth.config.AuthConfig;
import gusl.auth.config.GoogleSSOConfig;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.filter.model.GoogleSSOUserDO;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.roles.RoleHelper;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.roles.RoleBasedSecurityContext;
import gusl.auth.roles.SuperUserSecurityContext;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.utils.StringUtils;
import gusl.model.exceptions.UnAuthorizedException;
import gusl.model.exceptions.ValidationException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import static gusl.core.utils.StringUtils.isBlank;
import static gusl.core.utils.StringUtils.isNotBlank;
import static gusl.node.transport.HttpUtils.waitForFuture;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Singleton
public class GoogleSSOHelper extends AbstractAuthHelper implements AuthHelper {
    public static final String GOOGLE_SSO_TOKEN_NAME = "access_token";

    private final Map<String, GoogleSSOUserDO> theLoggedInUserMap = new ConcurrentHashMap<>();

    public AuthType getAuthType() {
        return AuthType.GOOGLE_SSO;
    }

    @Inject
    private AuthClient theAuthClient;

    @Inject
    private RoleCache theRoleCache;

    private GoogleSSOConfig theGoogleSSOConfig;

    @Override
    public int getPriority() {
        return 10;
    }

    @Override
    public boolean canAuthRequest(HttpServletRequest request) {
        logger.debug("{} canAuthRequest: {}",
                request.getRequestURI(),
                isNotBlank(request.getHeader(GOOGLE_SSO_TOKEN_NAME)));
        return isNotBlank(request.getHeader(GOOGLE_SSO_TOKEN_NAME));
    }

    @Override
    public boolean canAuthRequest(ContainerRequestContext request) {
        logger.debug("{} canAuthRequest: {}",
                request.getUriInfo().getPath(),
                isNotBlank(request.getHeaderString(GOOGLE_SSO_TOKEN_NAME)));
        return isNotBlank(request.getHeaderString(GOOGLE_SSO_TOKEN_NAME));
    }

    @Override
    public boolean canAuthRequest(TokenLoginRequestDO request) {
        return nonNull(request.getToken());
    }

    @Override
    public boolean canAuthRequest(Map<String, String> headers) {
        return isNotBlank(headers.get(GOOGLE_SSO_TOKEN_NAME));
    }

    @Override
    public ILoggedInUser getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            HttpServletRequest request
    ) throws GUSLErrorException {
        String token = request.getHeader(GOOGLE_SSO_TOKEN_NAME);
        String mediaType = request.getHeader(MEDIA_TYPE_HEADER);
        String orientation = request.getHeader(ORIENTATION_HEADER);
        if (isBlank(token)) {
            logger.warn("Google SSO token in header is blank");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        return theLoggedInUserMap.containsKey(token)
                ? validateNewToken(token, mediaType, orientation)
                : validateExistingToken(token, mediaType, orientation);
    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, ContainerRequestContext request) {
        return getLoggedInDetailsFor(loggedInCache, request.getHeaderString(GOOGLE_SSO_TOKEN_NAME));
    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, Map<String, String> headers) {
        return getLoggedInDetailsFor(loggedInCache, headers.get(GOOGLE_SSO_TOKEN_NAME));
    }

    private SecurityContext getLoggedInDetailsFor(LoggedInUserCache loggedInCache, String token) {
        final GoogleSSOUserDO ssoUser = theLoggedInUserMap.get(token);
        return isNull(ssoUser) ? null : ssoUser.getSecurityContext();
    }

    @Override
    public TokenLoginResponseDO tokenLogin(
            LoggedInUserCache loggedInCache,
            TokenLoginRequestDO request
    ) throws GUSLErrorException {
        ILoggedInUser loggedInUser = theLoggedInUserMap.containsKey(request.getToken())
                ? validateNewToken(request.getToken(), null, null)
                : validateExistingToken(request.getToken(), null, null);
        return TokenLoginResponseDO.builder()
                .id(loggedInUser.getId())
                .sessionToken(request.getToken())
                .username(loggedInUser.getUsername())
                .avatar(loggedInUser.getAvatar())
                .build();
    }

    private void scheduleCleanUp() {
        BackgroundThreadPoolExecutor.scheduleAtFixedRate(() -> {
            List<String> tokensToRemove = new ArrayList<>(1);
            for (Map.Entry<String, GoogleSSOUserDO> entry : theLoggedInUserMap.entrySet()) {
                GoogleSSOUserDO user = entry.getValue();
                if (user.hasExpired()) {
                    tokensToRemove.add(entry.getKey());
                }
            }
            tokensToRemove.forEach(theLoggedInUserMap::remove);
        }, 1, 1, TimeUnit.MINUTES);

    }

    @Override
    public void initialise(AuthConfig authConfig) {
        if (nonNull(authConfig)) {
            theGoogleSSOConfig = authConfig.getGoogleSSOConfig();
            if (nonNull(theGoogleSSOConfig)) {
                scheduleCleanUp();
            }
        }
    }

    private ILoggedInUser validateExistingToken(
            String token,
            String mediaType,
            String orientation
    ) throws GUSLErrorException {
        return validateNewToken(token, mediaType, orientation);
    }

    private ILoggedInUser validateNewToken(
            String token,
            String mediaTypeHeader,
            String orientationHeader
    ) throws GUSLErrorException {
        GoogleIdToken.Payload payload = verifyGoogleToken(token);

        final UserSecurityDO userSecurity = waitForFuture(theAuthClient
                .getUserSecurityByUsername(GetUserByUsernameRequestDO.of(payload.getEmail())));

        MediaType mediaType = MediaType.Desktop;
        if (StringUtils.isNotBlank(mediaTypeHeader)) {
            try {
                mediaType = MediaType.valueOf(mediaTypeHeader);
            } catch (IllegalArgumentException e) {
            }
        }

        String orientation = null;
        if (StringUtils.isNotBlank(orientationHeader)) {
            orientation = orientationHeader;
        }

        LoggedInUserDO loggedInUser = LoggedInUserDO.builder()
                .id(userSecurity.getId())
                .username(payload.getEmail())
                .nickname((String) payload.get("given_name"))
                .avatar((String) payload.get("picture"))
                .permissions(theRoleCache.getPermissionsForRoles(userSecurity.getRoleIds()))
                .roleIds(userSecurity.getRoleIds())
                .superUser(theRoleCache.isSuperUser(userSecurity.getRoleIds()))
                .authType(AuthType.GOOGLE_SSO)
                .allowedCohorts(userSecurity.getAllowedCohorts())
                .currentCohortId(getCurrentCohort(userSecurity.getAllowedCohorts()))
                .currentMediaType(mediaType)
                .orientation(orientation)
                .build();

        final SecurityContext securityContext;
        if (RoleHelper.isSuperUser(loggedInUser)) {
            securityContext = new SuperUserSecurityContext(loggedInUser.getUsername());
        } else {
            securityContext = new RoleBasedSecurityContext(loggedInUser.getUsername(), loggedInUser.getPermissions());
        }

        theLoggedInUserMap.put(token,
                GoogleSSOUserDO.builder()
                        .payload(payload)
                        .securityContext(securityContext)
                        .build());

        return loggedInUser;
    }

    // https://developers.google.com/identity/sign-in/web/backend-auth
    private GoogleIdToken.Payload verifyGoogleToken(String token) throws ValidationException {
        if (isNull(token)) {
            throw new ValidationException(AuthenticationErrors.ERR_GOOGLE_TOKEN_IS_NULL.getError());
        }

        try {

            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
                    .setAudience(Collections.singletonList(theGoogleSSOConfig.getClientId()))
                    .build();

            // This can throw a null pointer
            GoogleIdToken idToken = verifier.verify(token);

            if (nonNull(idToken)) {
                GoogleIdToken.Payload payload = idToken.getPayload();

                // Make sure its the same domain
                if (!theGoogleSSOConfig.getSecurityDomain().equals(payload.getHostedDomain())) {
                    // User not logged in.
                    throw new ValidationException(AuthenticationErrors.ERR_GOOGLE_TOKEN_INVALID_HOSTNAME.getError(payload.getHostedDomain()));
                }

                // Make sure it's not expired.
                if (payload.getExpirationTimeSeconds() * 1000 <= System.currentTimeMillis()) {
                    // We have expired
                    throw new ValidationException(AuthenticationErrors.ERR_GOOGLE_TOKEN_EXPIRED.getError(payload.getHostedDomain()));
                }

                if (!payload.getEmailVerified()) {
                    throw new ValidationException(AuthenticationErrors.ERR_GOOGLE_TOKEN_INVALID_EMAIL_VALIDATION.getError(payload.getEmail()));
                }

                return payload;

            } else {
                logger.warn("After google verify token is null");
                throw new ValidationException(AuthenticationErrors.ERR_GOOGLE_TOKEN_VERIFY.getError());

            }
        } catch (NullPointerException | IOException | GeneralSecurityException ex) {
            logger.warn("Unable to read token", ex);
            throw new ValidationException(AuthenticationErrors.ERR_GOOGLE_TOKEN_VALIDATION.getError());
        }
    }

}
