package gusl.auth.filter.authhelpers;

import gusl.auth.AuthType;
import gusl.auth.config.AuthConfig;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.jwt.JwtService;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.session.Session;
import gusl.model.exceptions.AccessDeniedException;
import gusl.model.exceptions.UnAuthorizedException;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;

import static gusl.core.utils.StringUtils.isNotBlank;
import static java.util.Objects.nonNull;

@Singleton
public class UsernamePasswordHelper extends AbstractJwtAuthHelper implements AuthHelper {

    private static final String USER_TOKEN_NAME = JwtService.USER_TOKEN_NAME;
    private static final String SESSION_TOKEN_NAME = JwtService.SESSION_TOKEN_NAME;

    public AuthType getAuthType() {
        return AuthType.USERNAME_PASSWORD;
    }

    @Override
    public int getPriority() {
        return 40;
    }

    @Override
    protected String getUserTokenName() {
        return USER_TOKEN_NAME;
    }

    @Override
    protected String getSessionTokenName() {
        return SESSION_TOKEN_NAME;
    }

    @Override
    public boolean canAuthRequest(HttpServletRequest request) {
        logger.debug("{} canAuthRequest: {}",
                request.getRequestURI(),
                isNotBlank(request.getHeader(getUserTokenName()))
                        && isNotBlank(request.getHeader(getSessionTokenName())));

        return isNotBlank(request.getHeader(getUserTokenName()))
                && isNotBlank(request.getHeader(getSessionTokenName()));
    }

    @Override
    public boolean canAuthRequest(ContainerRequestContext request) {
        logger.debug("{} canAuthRequest: {}",
                request.getUriInfo().getPath(),
                isNotBlank(request.getHeaderString(getUserTokenName()))
                        && isNotBlank(request.getHeaderString(getSessionTokenName())));

        return isNotBlank(request.getHeaderString(getUserTokenName()))
                && isNotBlank(request.getHeaderString(getSessionTokenName()));
    }

    @Override
    public boolean canAuthRequest(TokenLoginRequestDO request) {
        return nonNull(request.getUserToken()) && nonNull(request.getSessionToken());
    }

    @Override
    public boolean canAuthRequest(Map<String, String> headers) {
        return isNotBlank(headers.get(getUserTokenName()))
                && isNotBlank(headers.get(getUserTokenName()));
    }

    @Override
    public ILoggedInUser getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            HttpServletRequest request
    ) {
        return getLoggedInDetailsFor(loggedInCache,
                request.getHeader(getUserTokenName()),
                request.getHeader(getSessionTokenName()),
                request.getHeader(MEDIA_TYPE_HEADER),
                request.getHeader(ORIENTATION_HEADER),
                request.getPathInfo());
    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, ContainerRequestContext request) {
        logger.debug("getSecurityContextFor (requestContext) : {} {}", request.getMethod(), request.getUriInfo().getPath());
        return getSecurityContextFor(
                loggedInCache,
                request.getHeaderString(getUserTokenName()),
                request.getHeaderString(getSessionTokenName()),
                request.getHeaderString(MEDIA_TYPE_HEADER),
                request.getHeaderString(ORIENTATION_HEADER),
                nonNull(request.getUriInfo()) ? request.getUriInfo().getPath() : null);
    }

    @Override
    public SecurityContext getSecurityContextFor(LoggedInUserCache loggedInCache, Map<String, String> headers) {
        return getSecurityContextFor(
                loggedInCache,
                headers.get(getUserTokenName()),
                headers.get(getSessionTokenName()),
                headers.get(MEDIA_TYPE_HEADER),
                headers.get(ORIENTATION_HEADER),
                null);
    }

    private SecurityContext getSecurityContextFor(
            LoggedInUserCache loggedInCache,
            String userToken,
            String sessionToken,
            String mediaType,
            String orientation,
            String uri
    ) {
        return getJwtSecurityContextFor(loggedInCache, userToken, sessionToken,mediaType, orientation,uri);
    }

    @Override
    public void initialise(AuthConfig authConfig) {
        scheduleCleanUp();
    }

    @Override
    public TokenLoginResponseDO tokenLogin(LoggedInUserCache loggedInCache, TokenLoginRequestDO request) {
        return jwtTokenLogin(loggedInCache, request, null);
    }

    @Override
    public void validate(String userId, ILoggedInUser user, Session session, String uri) {
        if (!user.canLogin()) {
            logger.warn("User {} is not allowed to login", user.getId());
            throw new AccessDeniedException(AuthenticationErrors.CUSTOMER_DENIED.getError());
        }
        if (!session.getStatus().isActive()) {
            logger.warn("Invalid status on session token {} {} ", userId, session.getStatus());
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
    }

}
