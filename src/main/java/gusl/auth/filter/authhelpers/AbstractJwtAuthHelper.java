package gusl.auth.filter.authhelpers;

import gusl.annotations.form.MediaType;
import gusl.auth.client.AuthClient;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.filter.LoggedInUserCache;
import gusl.auth.jwt.JwtService;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.login.TokenLoginRequestDO;
import gusl.auth.model.login.TokenLoginResponseDO;
import gusl.auth.model.roles.RoleHelper;
import gusl.auth.model.security.SecurityUser;
import gusl.auth.model.session.CreateSessionRequestDO;
import gusl.auth.model.session.CreateSessionResponseDO;
import gusl.auth.model.session.Session;
import gusl.auth.model.session.SessionDO;
import gusl.auth.model.token.SessionTokenDO;
import gusl.auth.model.token.UserTokenDO;
import gusl.auth.roles.RoleBasedSecurityContext;
import gusl.auth.roles.SuperUserSecurityContext;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.StringUtils;
import gusl.model.exceptions.UnAuthorizedException;
import io.jsonwebtoken.Claims;

import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

public abstract class AbstractJwtAuthHelper extends AbstractAuthHelper {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private final Map<String, SecurityUser> theLoggedInUserMap = new ConcurrentHashMap<>();

    @Inject
    protected JwtService theJwtService;

    @Inject
    private AuthClient theAuthClient;

    protected abstract String getUserTokenName();

    protected abstract String getSessionTokenName();

    protected SecurityContext getJwtSecurityContextFor(
            LoggedInUserCache loggedInCache,
            String userToken,
            String sessionToken,
            String mediaType,
            String orientation,
            String uri
    ) {
        final ILoggedInUser loggedInUser = getLoggedInDetailsFor(loggedInCache, userToken, sessionToken, mediaType, orientation, uri);
        SecurityUser user = theLoggedInUserMap.get(sessionToken);

        if (isNull(user)) {
            user = new SecurityUser();
        }

        if (RoleHelper.isSuperUser(loggedInUser)) {
            logger.debug("{} -> SuperUserSecurityContext", loggedInUser.getUsername());
            user.setSecurityContext(new SuperUserSecurityContext(loggedInUser.getUsername()));
        } else {
            logger.debug("{} -> RoleBasedSecurityContext", loggedInUser.getUsername());
            user.setSecurityContext(new RoleBasedSecurityContext(loggedInUser.getUsername(), loggedInUser.getPermissions()));
        }

        user.setLoggedInUser(loggedInUser);
        theLoggedInUserMap.put(sessionToken, user);
        return user.getSecurityContext();
    }

    protected ILoggedInUser getLoggedInDetailsFor(
            LoggedInUserCache loggedInCache,
            String userToken,
            String sessionToken,
            String mediaType,
            String orientation,
            String uri
    ) {
        validate(userToken, sessionToken);
        UserTokenDO userTokenDo = getUserToken(userToken);
        SessionTokenDO sessionTokenDo = getSessionToken(sessionToken);
        validate(userTokenDo, sessionTokenDo);

        Optional<ILoggedInUser> user = loggedInCache.getUser(userTokenDo.getId());
        Optional<Session> session = loggedInCache.getSession(sessionTokenDo.getId(), sessionTokenDo.getSessionId());

        // we could add auto login here ... so valid user, old session
        validate(userTokenDo.getId(), user, session, uri);

        if (user.isPresent()) {
            if (StringUtils.isNotBlank(mediaType)) {
                try {
                    user.get().setCurrentMediaType(MediaType.valueOf(mediaType));
                } catch (IllegalArgumentException e) {
                    user.get().setCurrentMediaType(MediaType.Desktop);
                }
            } else {
                user.get().setCurrentMediaType(MediaType.Desktop);
            }

            if (StringUtils.isNotBlank(orientation)) {
                user.get().setOrientation(orientation);
            }
        }

        // InetAddress address = HttpUtils.getRemoteIpAddress(request);
//        final ILoggedInUser cacheRecord = loggedInCache.<ILoggedInUser>find(user.get().getId()).orElse(null);
//        if (nonNull(cacheRecord)) {
//            // cacheRecord.setUser(user.get());
//            cacheRecord.setSession(session.get());
//        }
//         return cacheRecord;
        return user.orElse(null);
    }

    private void validate(String userId, Optional<ILoggedInUser> user, Optional<Session> session, String uri) {
        if (user.isEmpty() || session.isEmpty()) {
            logger.warn("Failed to get user or session from cache - cannot be logged in", userId);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        validate(userId, user.get(), session.get(), uri);
    }

    public abstract void validate(String userId, ILoggedInUser user, Session session, String uri);

    protected TokenLoginResponseDO jwtTokenLogin(LoggedInUserCache loggedInCache, TokenLoginRequestDO request, String uri) {
        validate(request);
        UserTokenDO userTokenDo = getUserToken(request.getUserToken());

        ILoggedInUser user = loggedInCache.getUser(userTokenDo.getId()).orElse(null);

        try {
            if (isNull(user)) {
                user = loggedInCache.loginUser(userTokenDo.getId());
            }
        } catch (GUSLErrorException e) {
            logger.warn("Token login error userTokenDo id=[{}]", userTokenDo.getId(), e);
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
        if (isNull(user)) {
            logger.warn("Failed to find user");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }

        SessionTokenDO sessionTokenDo;
        if (isSessionTokenValid(request.getSessionToken())) {
            sessionTokenDo = getSessionToken(request.getSessionToken());
        } else {
            try {
                logger.debug("theAuthClient: {}", theAuthClient);

                logger.debug(" ............  Client {} .....  User {} ....", theAuthClient, user);

                final CreateSessionResponseDO response = theAuthClient.createSession(CreateSessionRequestDO.builder()
                        .userId(user.getId())
                        .username(user.getUsername())
                        .secretKey(userTokenDo.getSecretKey())
                        .build()).get();
                loggedInCache.add(response.getSession());

                sessionTokenDo = SessionTokenDO.builder()
                        .id(response.getId())
                        .sessionId(response.getSession().getId())
                        // .username(response.)
//                        .createdDate(response.getSession().getDateCreated())
//                        .expiryDate(response.getSession().getExpiryDate())
                        .build();
            } catch (InterruptedException | ExecutionException e) {
                if (e.getCause() instanceof GUSLErrorException) {
                    if ("record.not.found".equals(((GUSLErrorException) e.getCause()).getError().getMessageKey())) {
                        logger.warn("User [{}] secret key did not match - session not created", user.getId());
                        logger.temp("Secret key in token: {}", userTokenDo.getSecretKey());
                    } else {
                        logger.error("Error creating a session", e.getCause());
                    }
                } else {
                    logger.error("Error creating a session {}", e.getMessage());
                }
                throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
            }
        }
        validate(userTokenDo, sessionTokenDo);

        Optional<Session> session = loggedInCache.getSession(sessionTokenDo.getId(), sessionTokenDo.getSessionId());
        if (session.isEmpty()) {
            logger.warn("Session not found in cache");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }

        // we could add auto login here ... so valid user, old session
        validate(userTokenDo.getId(), user, session.get(), uri);
        return TokenLoginResponseDO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .avatar(user.getAvatar())
                .properties(user.getProperties())
                .userToken(request.getUserToken())
                .sessionToken(((SessionDO) session.get()).getSessionToken())
                .build();
    }

    protected UserTokenDO getUserToken(String userToken) {
        try {
            final Claims claims = theJwtService.parseToken(theJwtService.getCustomerSecurityKey(), userToken);
            return UserTokenDO.parseClaims(claims);
        } catch (UnAuthorizedException e) {
            logger.warn("Failed to parse user token {}", e.getMessage());
            logger.debug("Failed to parse user token {}", e.getMessage(), e);
            throw new ForbiddenException(AuthenticationErrors.PARSE_ERROR.getError().getMessage(), e);
        }
    }

    protected boolean isSessionTokenValid(String sessionToken) {
        try {
            final Claims claims = theJwtService.parseToken(theJwtService.getSessionSecurityKey(), sessionToken);
            SessionTokenDO.parseClaims(claims);
            return true;
        } catch (UnAuthorizedException e) {
            return false;
        }
    }

    protected SessionTokenDO getSessionToken(String sessionToken) {
        try {
            final Claims claims = theJwtService.parseToken(theJwtService.getSessionSecurityKey(), sessionToken);
            return SessionTokenDO.parseClaims(claims);
        } catch (UnAuthorizedException e) {
            logger.warn("Failed to parse session token {}", e.getMessage());
            logger.debug("Failed to parse session token {}", e.getMessage(), e);
            throw new ForbiddenException(AuthenticationErrors.PARSE_ERROR.getError().getMessage(), e);
        }
    }

    protected void validate(UserTokenDO userTokenDo, SessionTokenDO sessionTokenDo) {
        // some basic validation - this is also done in the PlayerAuthenticated Filter - so I don't think this is needed
        if (!sessionTokenDo.getId().equals(userTokenDo.getId())) {
            logger.error("Invalid Token - session token is not for customer");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }

    }

    protected void validate(TokenLoginRequestDO request) {
        if (isNull(request.getUserToken()) || isNull(request.getSessionToken())) {
            logger.warn("Missing userToken or sessionToken headers");
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }
    }

    protected void validate(String userToken, String sessionToken) {
        if (isNull(userToken) || isNull(sessionToken)) {
            logger.warn("Missing customerToken or sessionToken headers");
//            try {
//                throw new Exception("here");
//            } catch (Exception e) {
//                logger.error("here", e);
//            }
            throw new UnAuthorizedException(AuthenticationErrors.INVALID_TOKEN.getError());
        }

    }

    protected void scheduleCleanUp() {
        BackgroundThreadPoolExecutor.scheduleAtFixedRate(() -> {
            List<String> tokensToRemove = new ArrayList<>(1);
            for (Map.Entry<String, SecurityUser> entry : theLoggedInUserMap.entrySet()) {
                SecurityUser user = entry.getValue();
                // not sure how we are going to do this
//                if (user.hasExpired()) {
//                    tokensToRemove.add(entry.getKey());
//                }
            }
            tokensToRemove.forEach(theLoggedInUserMap::remove);
        }, 1, 1, TimeUnit.MINUTES);

    }

}
