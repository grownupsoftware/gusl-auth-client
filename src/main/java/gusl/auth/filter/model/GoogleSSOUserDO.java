package gusl.auth.filter.model;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import lombok.*;

import javax.ws.rs.core.SecurityContext;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GoogleSSOUserDO {
    private SecurityContext securityContext;
    private GoogleIdToken.Payload payload;
    // private BackofficeUserDO loggedInUser;

    public boolean hasExpired() {
        return ((payload.getExpirationTimeSeconds() * 1000) <= System.currentTimeMillis());
    }

}
