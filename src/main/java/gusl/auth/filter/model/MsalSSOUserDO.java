package gusl.auth.filter.model;

import gusl.core.tostring.ToString;
import lombok.*;

import javax.ws.rs.core.SecurityContext;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MsalSSOUserDO {

    private SecurityContext securityContext;
    private String username;
    private String name;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
