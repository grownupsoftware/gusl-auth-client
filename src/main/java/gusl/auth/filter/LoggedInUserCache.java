package gusl.auth.filter;

import gusl.auth.AuthType;
import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.model.session.Session;
import gusl.auth.model.session.SessionDO;
import gusl.core.exceptions.GUSLErrorException;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Contract
public interface LoggedInUserCache {

    Optional<Session> getSession(String userId, String id);

    Optional<ILoggedInUser> getUser(String id);

    Optional<ILoggedInUser> find(String id);

    void add(ILoggedInUser entity, UserSecurityDO userSecurity, SessionDO session);

    void add(SessionDO session);

    Set<AuthType> getAuthTypes();

    ILoggedInUser loginUser(String id) throws GUSLErrorException;

    void loadNearCache();

    List<String> getUserPermissions(String id);

    void killSessions(String userId);

    AuthType getDefaultAuthType();

}
