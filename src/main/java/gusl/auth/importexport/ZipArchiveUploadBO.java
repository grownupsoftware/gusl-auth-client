package gusl.auth.importexport;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class ZipArchiveUploadBO {
    @UiField(type = FieldType.file_upload, suffix = "*.zip")
    private String filename;

}
