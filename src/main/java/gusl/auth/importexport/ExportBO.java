package gusl.auth.importexport;


import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.model.nodeconfig.OperationalMode;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ExportBO<T> {
    private LoggedInUserDO loggedInUser;
    private LocalDateTime timestamp;
    private OperationalMode mode;
    private List<T> exportedEntities = new ArrayList<>();

    public List<T> getExportedEntities() {
        return exportedEntities;
    }

    protected void setExportedEntities(List<T> exportedEntities) {
        this.exportedEntities = exportedEntities;
    }

    public void addExportedEntity(T entity) {
        this.exportedEntities.add(entity);
    }

    public ExportBO() {
    }

    public ExportBO(final LoggedInUserDO loggedInUser, OperationalMode mode, T... exportedEntities) {
        if (Objects.isNull(exportedEntities)) {
            return;
        }

        for (T entity : exportedEntities) {
            this.exportedEntities.add(entity);
        }
        this.loggedInUser = loggedInUser;
        this.mode = mode;
        this.timestamp = LocalDateTime.now(ZoneId.of("UTC"));
    }

    public ExportBO(final LoggedInUserDO loggedInUser, OperationalMode mode, List<T> exportedEntities) {
        if (Objects.isNull(exportedEntities)) {
            return;
        }

        this.exportedEntities.addAll(exportedEntities);
        this.loggedInUser = loggedInUser;
        this.mode = mode;
        this.timestamp = LocalDateTime.now(ZoneId.of("UTC"));
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LoggedInUserDO getUser() {
        return loggedInUser;
    }

    public void setUser(final LoggedInUserDO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public OperationalMode getMode() {
        return mode;
    }

    public void setMode(OperationalMode mode) {
        this.mode = mode;
    }
}
