package gusl.auth.importexport;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.IOUtils;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static java.util.Objects.nonNull;

@Service
@CustomLog
public class ZipArchiveManager {

    private ObjectMapper theMapper = ObjectMapperFactory.getDefaultObjectMapper();

    public <T> StreamingOutput compress(String filename, T exportedEntity) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream out = new ZipOutputStream(baos)) {
            out.putNextEntry(new ZipEntry(filename));
            theMapper.writeValue(out, exportedEntity);
        } catch (IOException ex) {
            logger.error("Error creating zip entry", ex);
        }
        return (OutputStream output) -> {
            try {
                output.write(baos.toByteArray());
                output.flush();
            } catch (Exception e) {
                throw new WebApplicationException(e);
            }
        };
    }

    public <T> StreamingOutput compress(String filename, List<T> content) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream out = new ZipOutputStream(baos)) {
            out.putNextEntry(new ZipEntry(filename));
            theMapper.writeValue(out, content);
        } catch (IOException ex) {
            logger.error("Error creating zip entry", ex);
        }
        return (OutputStream output) -> {
            try {
                output.write(baos.toByteArray());
                output.flush();
            } catch (Exception e) {
                throw new WebApplicationException(e);
            }
        };
    }

    public <T> StreamingOutput nonCompress(List<T> content) {
        return (OutputStream output) -> {
            try {
                output.write(theMapper.writeValueAsString(content).getBytes(StandardCharsets.UTF_8));
                output.flush();
            } catch (Exception e) {
                throw new WebApplicationException(e);
            }
        };
    }

    public <T> StreamingOutput nonCompress(String content) {
        return (OutputStream output) -> {
            try {
                output.write(content.getBytes(StandardCharsets.UTF_8));
                output.flush();
            } catch (Exception e) {
                throw new WebApplicationException(e);
            }
        };
    }

    public String uncompress(String filename, InputStream inputStream) {
        ZipInputStream zis = new ZipInputStream(inputStream);
        ZipEntry entry;
        String contents = null;

        try {
            while ((entry = zis.getNextEntry()) != null) {
                if (nonNull(entry.getName()) && entry.getName().startsWith(getFilenamePrefix(filename))) {
                    try (ZippedFileInputStream entryStream = new ZippedFileInputStream(zis)) {
                        contents = IOUtils.inputStreamAsString(entryStream);
                    }
                } else {
                    while (zis.available() > 0) {
                        zis.read();
                    }

                }
            }
        } catch (IOException e) {
            logger.error("Failed to uncompress file");
            throw new WebApplicationException(e);
        }

        return contents;
    }

    private String getFilenamePrefix(String filename) {
        return filename.substring(0, filename.indexOf('_'));
    }
}
