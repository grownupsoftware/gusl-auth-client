package gusl.auth.importexport;

import com.fasterxml.jackson.core.type.TypeReference;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.ObjectMapperFactory;
import gusl.model.nodeconfig.NodeDetails;
import lombok.CustomLog;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static java.util.Objects.isNull;

@Service
@CustomLog
public class ImportExportHelper {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh-mm");

    private static final String ARCHIVE_ENTRY = "entity.json";

    @Inject
    private ZipArchiveManager theArchiveManager;

    @Inject
    private NodeDetails nodeDetails;

    public Response buildResponse(StreamingOutput stream, String fileName) {
        Response.ResponseBuilder responseBuilder = Response.ok(stream);
        responseBuilder.header("Access-Control-Expose-Headers", "export-filename,Content-Disposition");

        responseBuilder.header("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        responseBuilder.header("export-filename", fileName);
        return responseBuilder.build();
    }

    public <T> Response buildResponseForCsv(final LoggedInUserDO loggedInUser, String filename, List<T> entities) {
        if (isNull(entities) || entities.isEmpty()) {
            return Response.serverError().build();
        }
        StreamingOutput output = theArchiveManager.nonCompress(entities);
        return buildResponse(output, getExportFileNameForCsv(filename));
    }

    public <T> Response buildResponseForCsv(final LoggedInUserDO loggedInUser, String filename, String content) {
        if (isNull(content) || content.isEmpty()) {
            return Response.serverError().build();
        }
        StreamingOutput output = theArchiveManager.nonCompress(content);
        return buildResponse(output, getExportFileNameForCsv(filename));
    }

    public <T> Response buildResponseForAll(LoggedInUserDO loggedInUser, List<T> entities) {
        if (isNull(entities) || entities.isEmpty()) {
            return Response.serverError().build();
        }
        StreamingOutput output = theArchiveManager.compress(ARCHIVE_ENTRY,
                new ExportBO<>(loggedInUser, nodeDetails.getOperationalMode(), entities));
        return buildResponse(output, getExportFileName(entities.get(0)));
    }

    private <T> String getExportFileName(T entity) {
        return "export_" + entity.getClass().getSimpleName().toLowerCase() + "_" + dateFormat.format(new Date()) + ".zip";
    }

    public String getExportFileNameForCsv(String name) {
        return "export_" + name + "_" + dateFormat.format(new Date()) + ".csv";
    }

    public <T> List<T> extractImportEntities(
            InputStream uploadedInputStream,
            FormDataContentDisposition fileDetail,
            FormDataBodyPart bodyPart,
            Class<T> exportBoClass
    ) throws GUSLErrorException {
        try {
            bodyPart.setMediaType(MediaType.APPLICATION_JSON_TYPE);

            ZipArchiveUploadBO uploadedFile = bodyPart.getValueAs(ZipArchiveUploadBO.class);
            ExportBO<T> allEntities = ObjectMapperFactory.getDefaultObjectMapper()
                    .readValue(
                            theArchiveManager.uncompress(uploadedFile.getFilename(), uploadedInputStream),
                            new TypeReference<ExportBO<T>>() {
                            });
            return allEntities.getExportedEntities();
        } catch (Throwable ex) {
            logger.error("Failed to upload file", ex);
            throw new GUSLErrorException(AuthenticationErrors.FAILED_TO_UPLOAD_FILE.getError());
        }

    }
}
