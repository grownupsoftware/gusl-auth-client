package gusl.auth.importexport;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

/**
 * @author grantwallace
 */
public class ZippedFileInputStream extends InputStream implements Closeable {

    private ZipInputStream is;

    public ZippedFileInputStream(ZipInputStream is) {
        this.is = is;
    }

    @Override
    public int read() throws IOException {
        return is.read();
    }

    @Override
    public void close() throws IOException {
        is.closeEntry();
    }

}
