package gusl.auth.storage;

import gusl.core.utils.IOUtils;
import gusl.core.utils.SystemPropertyUtils;
import gusl.node.jersey.JerseyHttpConfig;
import gusl.node.jersey.JerseyNodeClient;
import lombok.CustomLog;
import org.junit.Test;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static gusl.node.jersey.JerseyHttpConfig.*;

/**
 * @author dhudson
 * @since 05/08/2022
 */
@CustomLog
public class DownloadTester {

    private final static String FILENAME = "GeoIP2-City.mmdb";


    @Test
    public void downloadTest() {

        System.setProperty("ApplicationRepository", "repo");
        File repoLocation = new File(SystemPropertyUtils.getRepositoryLocation());

        boolean downloadRequired = false;

        if (!repoLocation.exists()) {
            logger.info("Creating Application Repo {}", repoLocation.getAbsolutePath());
            repoLocation.mkdirs();
        }

        File database = new File(repoLocation, FILENAME);

        if (!database.exists()) {
            JerseyNodeClient client = new JerseyNodeClient(JerseyHttpConfig.builder()
                    .socketConfig(defaultSocketConfig())
                    .properties(defaultClientProperties())
                    .requiresConnectionPool(false)
                    .readTimeout(READ_TIMEOUT_DEFAULT)
                    .connectionTimeout(CONNECT_TIMEOUT_DEFAULT)
                    .maxConnections(MAX_CONNECTIONS_DEFAULT)
                    .build());

            WebTarget webTarget = client.getClient().target("https://storage.cloud.google.com/gusl-images/GeoIP2-City.mmdb?authuser=2");

            Invocation.Builder builder = webTarget.request(MediaType.APPLICATION_OCTET_STREAM_TYPE);

            Response response = builder.get();

            File db = new File("GeoIP2-City.mmdb");
            try {
                FileOutputStream fos = new FileOutputStream(db);
                IOUtils.copyStream(response.readEntity(InputStream.class), fos);
                logger.info("Response {}", response);
            } catch (IOException ex) {
                logger.warn("Nope ...");
            }

            client.close();
        }
    }
}
