![Alt text](https://www.googleapis.com/download/storage/v1/b/gusl-images/o/logo.png?generation=1588764693002370&alt=media)
# gusl-auth-client

Project that understand GEOIP and GEO Fences as well as logging in.

## Requirements
1. Java 11
2. Gradle 7

## Getting Started

```groovy

dependencies {
    implementation "co.gusl:gusl-auth-model:${guslVersion}"
    api "co.gusl:gusl-auth-client:${guslVersion}"
}

war {

    with copySpec {
        (configurations.runtimeClasspath).collect {
            if (it.toString().indexOf("gusl-auth-client") > 0) {
                from(zipTree(it)) {
                    include 'GeoIP2-City.mmdb'
                    into 'WEB-INF/classes'
                }
            }
        }
    }
}
```

If you want to use locaLocalFile then GeoIP2-City.mmdb will need to be added to resources.



